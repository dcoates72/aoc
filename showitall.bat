clear

@echo off

set argCount=0
for %%x in (%*) do (
   set /A argCount+=1
   set argVec[!argCount!]=%%~x
)

for /L %%i in (1,1,%argCount%) do echo %%i- !argVec[%%i]!
echo on

"c:\Program Files (x86)\PsychoPy2\pythonw.exe" -c "import util; util.snarf_all('%*')"