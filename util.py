import numpy as np
#import params2 as params
#import params2 as params2
#import rfparams as rfparams
import sys
import uns
import os 
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter
import datetime
import time
from scipy import ndimage
import statsmodels.api as sm
lowess = sm.nonparametric.lowess

def buildtrialseq( vals, ntrials):
	return np.random.permutation( np.tile( vals, np.ceil( float(ntrials)/len(vals) ) ))[0:ntrials]

def unhist( data ):
	vals = np.unique( data )
	indices=[np.where( data==aval) for aval in vals]
	return vals, indices

def dumpvars( module, f=None ):
	#eval( "import %s"% module.__name__ ) #TODO: need to import the file above!? hack

	if f==None:
		f=sys.stdout

	vas=dir( module )
	for avar in vas:
		if avar[0] != "_":
			#f.write("#%s=%r\n"%(avar,eval("%s.%s"%(module.__name__,avar) ) ) )
			thevar=getattr(module,avar)
			if hasattr(thevar, '__iter__' ):
				theval="[%s]"%( ','.join(map(str,thevar)))
			else:
				theval="%s"%(thevar)
			f.write("#%s=%s\n"%(avar,theval) )

	# add a timestamp variable.
	timestamp=datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S %p")
	f.write("#datetime=%s\n"%(str(timestamp)))

def deg2pix(deg):
	pixels_per_cm = params.screendim[1]/float(params.screen_height_cm)
	desired_cm = np.tan( deg * np.pi / 180.0 ) * params.distance_cm
	desired_pixels = desired_cm * pixels_per_cm
	return desired_pixels

def cm2deg(cm):
	degs = np.arctan( cm / params.distance_cm ) / np.pi * 180.0
	return degs

def cm2pix(cm):
	pixels_per_cm = params.screendim[1]/float(params.screen_height_cm)
	pix = cm*pixels_per_cm
	return pix

#minslope=0.005
#maxslope=0.5
def fixparms(p, B=100):
	#slope_recover = np.log10(p[1]/minslope) / np.log10(maxslope/minslope)
	slope_recover=slope2(p[1])
	#return p[0],0.5*np.log10(p[1]*B/5.0)-np.log10(minslope_div)/2.0
	return p[0]*B, slope_recover*B

# Convert slope in linear range (0,1) to 0.005,0.05 (log steps)
def slope2(sigma, minslope=0.005, maxslope=0.5):
	sigma = minslope * 10**(sigma/maxslope)
	return sigma

def PF(x,p, B=100):
	(B_0,B_1)=np.copy( p ) # careful: i've had bugs when aliasing the opt. params
	lapse=1./8
	#B_1 = 0.5 * np.log10( B_1 / 5.0 ) + 1.5 # do (Strasburger, #14) range conversion if necessary
	#B_1 = 0.5 * np.log10( B_1 / 5.0 ) + 1.5 # do (Strasburger, #14) range conversion if necessary
	B_1 = slope2( B_1 )
	pf=1./(1.+np.exp( (B_0-x)/B_1))
	return lapse+(1.-lapse)*pf

def estPF( p, c,ic ):
	#LL=np.sum( np.concatenate( (np.log(PF(c,p)), np.log( 1.0-PF(ic,p) ) ) ) )
	wrongs=np.log( 1.0-PF(ic,p))
	wrongs=1.0-PF(ic,p)
	wrongs[wrongs==0] = np.finfo('float').eps # fix to change anything that was PF=1
	wrongs=np.log( wrongs )
	LL=np.sum(np.log(PF(c,p))) + np.sum( wrongs )
	#print c
	#print ic
	#print -LL, p
	return -LL

def grids( c, ic, numgrid=21, doplot=False):
	P0 = np.linspace(0,1.0,numgrid)
	P1 = np.linspace(0,1.0,numgrid)
	#(X,Y)=
	LLs = np.array( [[estPF( (x,y),c,ic) for x in P0] for y in P1] )

	if doplot:
		plt.figure()
		plt.imshow( np.max(LLs)/LLs, interpolation='none', extent=(0,1,0,1) )
		plt.colorbar()

	return LLs

import matplotlib.pyplot as plt
def plotfit( p,c,ic, newfig=True, colr='b' ):
	if newfig: plt.figure()
	plt.plot( ic+np.random.normal(size=len(ic))/10000., np.zeros(len(ic))+np.random.normal(size=len(ic))/100, 'rx' )
		#plt.plot( cor+np.random.normal(size=len(cor))/50, x_all[cor], 'go' )
	plt.plot( c+np.random.normal(size=len(c))/10000., np.ones(len(c))+np.random.normal(size=len(c))/100, 'go' )
	plt.ylim(-0.1,1.1 )
	plt.xlim(0.0,1.0 )
	xr=np.linspace( np.min(ic), np.max(c) )
	plt.plot( xr, PF(xr, p), color=colr )

	# need "uns" unhist, which lets pass in values at which to evaluate, rather than doing unique
	all_uns = np.unique( np.concatenate( ( c, ic ) ) )
	num_c=np.array( uns.unhist( c, all_uns )[1], dtype='float' )
	num_ic=np.array( uns.unhist( ic, all_uns )[1], dtype='float' )

	n=num_c+num_ic
	p_hat = num_c/n
	conf95 = 1.96 * np.sqrt(p_hat*(1.-p_hat)/n)
	se_hat = np.sqrt(p_hat*(1.-p_hat)/n)

	plt.errorbar( all_uns, p_hat, yerr=se_hat, marker='o', ls='', color=colr )
	plt.grid( True )

import scipy.optimize
testC=np.array([0.5,0.75,1.0])
testIC=np.array([0.1,0.25,0.5])
def mlpf(c,ic,inits=(0.1,0.5)):
	return scipy.optimize.fmin_tnc( estPF, x0=inits, args=(c,ic), approx_grad=True, bounds=( (-1,100), (0.000001,1) ), disp=False ) 

# Find a unique filename by appending a number to the session
def get_unique_filename(filename_prefix):
	blockidx = 0
	gotunique = False
	while gotunique==False:
		outfilename = filename_prefix % blockidx
		try:
			if os.path.exists(outfilename):
				blockidx += 1
			else:
				gotunique = True
		except:
			try:
				os.mkdir('results')
				print ("Created new results directory.")
			except:
				print( "Fatal problem with results directory. Exiting." )
				sys.exit(-1)
	print (outfilename)
	return outfilename

def calibrate_timing(win, textstim, event, size=30, numloops=60, avg_range=(20,30), message_after="Ready. Hit any key."):
# Calibrate by seeing how long X redraws takes
	textstim.setHeight(size)
	msg = 'Calibrating...'
	textstim.setText( msg.upper() ) # sloan needs to be uppercase
	for i in np.arange(numloops):
		textstim.draw()
		win.flip()
	savetimes = win.frameIntervals
	fliprate = np.mean( savetimes[avg_range[0]:avg_range[1]] )
	textstim.setText( message_after.upper()  )
	textstim.draw()
	win.flip()
	event.waitKeys()
	return fliprate

def draw1(duration, stims, myWin, fliprate, ramp=False, mask=False, trial_cycles=1):
	# Display for a certain number of "flips," as close to duration as technically possible
	myWin.blendMode='avg'
	if (duration > 0):
		numflips = int( duration / fliprate )
		if numflips<1:
			numflips=1
		for angle in np.linspace( 0, np.pi*(trial_cycles/1.0), numflips ):
			contr = -0.5 + -0.5 * abs(np.sin( angle) )
			[stim.draw() for stim in stims]
			myWin.blendMode='avg'
			myWin.flip()
	return numflips * fliprate

# IF when=0 donothing, just return
def draw_wait( win, core, what, duration=0 ):
	if duration>0:
		[athing.draw() for athing in what]
		win.blendMode='avg'
		win.flip()
		core.wait(duration)

def snarf_all(fnames, sizes=None, pdfpltname=None, doplot=True, color=None, alpha=1.0, logx=False, do_plot_show=True, acuity_plot=False, normx=False, normy=False, pix_per_arcmin=-1.0):
	# do_plot_show determines whether to automatically  call plt.show()
	# IE, necessary for the command line batch files to display the plot.
	data=np.array([1,1])
	lins = []
	fcount = 0
	print ("Files looked at:")

	if isinstance(fnames, list):
		fnames = ' '.join(fnames)

	for fn in fnames.split(' '):
		print ('%02d: %s'%(int(fcount), os.path.basename(fn)))
		fil=open(fn,'rt',encoding = "ISO-8859-1")
		lins.extend(fil.readlines())
		fil.close()
		fcount += 1


	lins.sort()
	# strip the comment block:
	comments_until=max([nlin*(alin[0]=='#') for nlin,alin in enumerate(lins)])
	data=np.array([alin.split(',') for alin in lins[comments_until+1:] ])
	print ( 'Data: %s' % str(data.shape) )

	# pad with zeros so that sorting works, since those are alphabetic fields
	for dat in data:
		if acuity_plot:
			# total hack to do acuity plot: switch the two fields in the data
			dat[3] = '%04d'%int(dat[7])     # spacing, an int (pixels) or float (bar fraction)
			dat[7] = '%02d'%int(dat[2])       # size, an int
			dat[2] = dat[3]
		else:
			dat[7] = '%04d'%int(dat[7])     # spacing, an int (pixels) or float (bar fraction)
			dat[2] = '%02d'%int(dat[2])       # size, an int


	levels_size=np.unique(data[:,2])
	ignore_these_sizes =[]
	if sizes != None:
		for s in sizes:
			ss = '%02d'%int(s)
			ignore_these_sizes.extend([i for i, x in enumerate(levels_size) if x == ss])

		levels_size=np.delete(levels_size, ignore_these_sizes)

	print ( 'Sizes: %s' % str(levels_size) )
	levels_spacing=np.unique(data[:,7])
	print ( 'Spacings: %s' % str(levels_spacing) )

	unflanked_sentinel='5000'
	unflanked_present=False
	ncor=np.zeros( len(levels_spacing) )
	outoff=np.zeros( len(levels_spacing) )

	for nlevel_size,alevel_size in enumerate(levels_size):
		for nlevel_spacing,alevel_spacing in enumerate(levels_spacing):
			match_size=(data[:,2]==alevel_size )
			match_spac=(data[:,7]==alevel_spacing )
			indices= np.where( np.all((match_size, match_spac), 0) )[0]

			outoff[nlevel_spacing] = len( indices )
			ncor[nlevel_spacing] = len( np.where( data[indices,0]==data[indices,1] )[0] )
			if int(alevel_spacing)<1000:
				last_flanked=nlevel_spacing
			else:
				#this only works for a single unflanked trial, hacky...
				unflanked_present=nlevel_spacing

		last_flanked += 1 # since all the ranges go to one past this number..
		levels_flanked=levels_spacing[:last_flanked]

		# remove the "false" spacings (spacings where there is data at other sizes, but not at the current size)
		outoff_indices = [i for i, x in enumerate(outoff) if x == 0]
		percs=np.delete(ncor[:last_flanked], outoff_indices)
		outoff_shortened=np.delete(outoff[:last_flanked],outoff_indices)
		levels_flanked_shortened = np.delete(levels_flanked, outoff_indices)

		pcor=percs/outoff_shortened #+np.random.normal(scale=0.03,size=len(levels_spacing[:last_flanked])-len(outoff_indices))

		# Convert levels to float array:
		levels_flanked_shortened=np.array( levels_flanked_shortened, dtype='float')

		if doplot:
				ax=plt.gca()
				if normx:
					horiz_shift=1./float(alevel_size)
				else:
					horiz_shift=1.0

				# "Normalize" plot so that Y range goes from 0.0 to 1.0
				if normy:
					ymin=np.min(pcor)
					ymax=np.max(pcor)
					vert_shift=1.0/(ymax-ymin) 
					pcor_adjusted = (pcor-ymin)*vert_shift# + np.min(pcor)
				else:
					pcor_adjusted=pcor # don't do anything special
				
				if pix_per_arcmin>0.0:
					label=pretty_pix(int(alevel_size),pix_per_arcmin)
				else:
					label=alevel_size
				if color==None:
					lins=plt.plot(levels_flanked_shortened*horiz_shift, pcor_adjusted, 'o-', label=label, alpha=alpha)
				else:
					lins=plt.plot(levels_flanked_shortened*horiz_shift, pcor_adjusted, 'o-', label=label, color=color, alpha=alpha)

			# now do unflanked
				if unflanked_present:
					trans = ax.get_yaxis_transform() # X-axis is in relative coords (0..1), yaxis in data coords
					ax.errorbar(0.95, ncor[unflanked_present]/outoff[unflanked_present],  marker='s', transform=ax.transAxes, ms=10, color=lins[0].get_color() )
	if doplot:
		plt.legend(loc='best')
		plt.ylim(-0.1,1.1)
		ax=plt.gca()

		if logx:
			plt.semilogx()
			ax.xaxis.set_major_formatter( ScalarFormatter() )

		if pdfpltname is None:
			base = os.path.splitext(fnames.split(' ')[0])[0]
			title = '%s averaging %d files' % (os.path.basename(base), fcount)
			plt.title(title)

			if acuity_plot:
				pdfname = '%s_average_%s.pdf' % (base, "acuityplt")
				plt.xlabel('sizes')
			else:
				pdfname = '%s_average_%s.pdf' % (base, "spacingplt")
				plt.xlabel('spacings')
		else:
			pdfname=pdfpltname
			if not pix_per_arcmin: # title overlaps with upper axis 
				plt.title(os.path.basename(pdfpltname))
			if acuity_plot:
				plt.xlabel("size (pix)")
			else:
				if normx:
					plt.xlabel('E-E spacing (mult. of MAR)')
				else:
					plt.xlabel('E-E Spacing (pix)')

				if normy:
					plt.ylabel("Normalized prop. correct")
				else:
					plt.ylabel("Proportion correct")

		plt.grid( True )

		if (pix_per_arcmin>0.0) and (not normx):
			upperax( None, pix_per_arcmin )
		#plt.xlim( -0.1,100 )
		ax.margins(0.15) # add whitespace around numerical part of plot
		plt.savefig(pdfname)
		if do_plot_show:
			plt.show()

	return data, outoff, ncor



def snarf(fname, doplot=True, color=None, alpha=1.0, logx=False, do_plot_show=True, acuity_plot=False):
	# do_plot_show determines whether to automatically  call plt.show()
	# IE, necessary for the command line batch files to display the plot.

	fil=open(fname,'rt',encoding = "ISO-8859-1")
	lins=fil.readlines()
	fil.close()

	# strip the comment block:
	comments_until=max([nlin*(alin[0]=='#') for nlin,alin in enumerate(lins)])
	data=np.array([alin.split(',') for alin in lins[comments_until+1:] ])

	# pad with zeros so that sorting works, since those are alphabetic fields
	for dat in data:
		if acuity_plot:
			# total hack to do acuity plot: switch the two fields in the data
			dat[3] = '%04d'%int(dat[7])     # spacing, an int (pixels) or float (bar fraction)
			dat[7] = '%02d'%int(dat[2])       # size, an int
			dat[2] = dat[3]
		else:
			dat[7] = '%04d'%int(dat[7])     # spacing, an int (pixels) or float (bar fraction)
			dat[2] = '%02d'%int(dat[2])       # size, an int

	levels_size=np.unique(data[:,2])
	levels_spacing=np.unique(data[:,7])

	unflanked_sentinel='5000'
	unflanked_present=False

	ncor=np.zeros( len(levels_spacing) )
	outoff=np.zeros( len(levels_spacing) )

	last_flanked=None
	for nlevel_size,alevel_size in enumerate(levels_size):
		for nlevel_spacing,alevel_spacing in enumerate(levels_spacing):
			match_size=(data[:,2]==alevel_size )
			match_spac=(data[:,7]==alevel_spacing )
			indices= np.where( np.all((match_size, match_spac), 0) )[0]
			outoff[nlevel_spacing] = len( indices )
			ncor[nlevel_spacing] = len( np.where( data[indices,0]==data[indices,1] )[0] )
			if int(alevel_spacing)<1000:
				last_flanked=nlevel_spacing
			else:
				#this only works for a single unflanked trial, hacky...
				unflanked_present=nlevel_spacing

		# The following can happen if there are no unflanked trials
		if last_flanked==None:
			last_flanked=int(alevel_spacing)
		
		last_flanked += 1 # since all the ranges go to one past this number..
		levels_flanked=levels_spacing[:last_flanked]
		percs=ncor[:last_flanked]
		pcor=ncor[:last_flanked]/outoff[:last_flanked]+\
				np.random.normal(scale=0.03,size=len(levels_spacing[:last_flanked]))

		if doplot:
				ax=plt.gca()
				if color==None:
					lins=plt.plot(levels_flanked, pcor, 'o-', label=str(alevel_size), alpha=alpha)
				else:
					lins=plt.plot(levels_flanked, pcor, 'o-', label=str(alevel_size), color=color, alpha=alpha)

			# now do unflanked
				if unflanked_present:
					trans = ax.get_yaxis_transform() # X-axis is in relative coords (0..1), yaxis in data coords
					ax.errorbar(0.95, ncor[unflanked_present]/outoff[unflanked_present],  marker='s', transform=ax.transAxes, ms=10, color=lins[0].get_color() )

		# coming at the end. Hacky...
		#if doplot:
			#ax=plt.gca();
			#print ax.ylim()
			#if int(alevels_spacing)>5000:
				 #ax.errorbar(0.95, ncor,  marker='s',

	if doplot:
		plt.legend(loc='best')
		plt.ylim(-0.1,1.1)
		ax=plt.gca()

		if logx:
			plt.semilogx()
			ax.xaxis.set_major_formatter( ScalarFormatter() )

		base = os.path.splitext(fname)[0]
		plt.title(os.path.basename(base))
		if acuity_plot:
			pdfname = '%s_%s.pdf' % (base, "acuityplt")
			plt.xlabel('sizes')
		else:
			pdfname = '%s_%s.pdf' % (base, "spacingplt")
			plt.xlabel('spacings')

		plt.grid( True )
		#plt.xlim( -0.1,100 )
		ax.margins(0.15) # add whitespace around numerical part of plot
		plt.savefig(pdfname)
		if do_plot_show:
			plt.show()

	return data, outoff, ncor

# Versions of concatenation code that use Pandas to do all the aggregation
import pandas as pd
def do1(fname):
    res=snarf(fname, doplot=False);   
    fields=['resp', 'targ', 'size', 'l', 'r','u','d','spacing','duration', 'response_time' ]
    df = pd.DataFrame(res[0], columns=fields )
    df['cor'] = pd.Series((df.resp==df.targ)+0)
    df['spacing'] = pd.Series( df.spacing.apply(float) ) # convert to int
    blockmean = df.groupby(['size', 'spacing']).mean()
    blocksize = df.groupby(['size', 'spacing']).count()
    return blockmean.reset_index(drop=False),blocksize.reset_index(drop=False)

def do1_raw(fname):
    res=snarf(fname, doplot=False);   
    fields=['resp', 'targ', 'size', 'l', 'r','u','d','spacing','duration', 'response_time' ]
    df = pd.DataFrame(res[0], columns=fields )
    df['cor'] = pd.Series((df.resp==df.targ)+0)
    df['spacing'] = pd.Series( df.spacing.apply(float) ) # convert to int
    return df

def pixmin(pix,pix_per_arcmin=1.0):
	return pix/pix_per_arcmin

def pretty_pix(pix,pix_per_arcmin=1.0,add_decimal=True):
    arcmin=pixmin(pix,pix_per_arcmin)
    if add_decimal:
        s="%dpx (%02.2f') 20/%0.1f"%(pix,arcmin,arcmin*20.0)
    else:
        s="%dpx (%02.2f')"%(pix,arcmin)
    return s

# Added 2017-Feb-09 to try to de-tangle 
def dolist_raw( which ):
	for nfil,afil in enumerate(which):
		res=do1_raw(afil)
		res['filenum']=nfil
		try:
			raw=pd.concat( [raw, res ] )
		except:
			raw=res

	major='size'
	minor='spacing'

	raw = raw.sort_values( major)

	# I'm not sure what drop=False does, but need reset_index to go back to a dataframe
	means=raw.groupby([major, minor]).mean()['cor'].reset_index(drop=False)
	stds=raw.groupby([major, minor]).std()['cor'].reset_index(drop=False)
	stds=stds.rename(index=str, columns={'cor':'stds'})
	counts=raw.groupby([major, minor]).count()['cor'].reset_index(drop=False)
	counts=counts.rename(index=str, columns={'cor':'N'})
	data_binom=pd.merge( means, pd.merge( stds, counts)) #, stds )

    # Separated by block:
	cor2=raw.groupby([major, minor,"filenum"]).sum()['cor'].reset_index(drop=False)
	counts2=raw.groupby([major, minor,"filenum"]).count()['cor'].reset_index(drop=False)
	counts2=counts2.rename(index=str, columns={'cor':'N'})
	b2=pd.merge( cor2, counts2 )

	# Return some emptiness with compatibility for previous functions.
	return data_binom,raw,b2

def dolist( which, doplot=True, ls='-', flanked_acuity=False, pix_per_arcmin=1.0, figsize=(8,6), fig=None, ignore_sizes=[], jitter_inc=0.1, scalex=False, lbl_prefix="MAR ", plot_smoothed=True, smooth_sigma=0.75, plot_diff=False, norm='', frac_lo=0.5):

    if fig==None:
    	fig = plt.figure( figsize=figsize)
    ax1 = fig.add_subplot(111)

    tots=[]#do1(which[0])
    totc=[]
    for afil in which:
        res=do1(afil)
        try:
            tots=pd.concat( [tots, res[0] ] )
            totc=pd.concat( [totc, res[1] ] )
        except:
             tots,totc=res

    if flanked_acuity:
        major='spacing'
        minor='size'
    else:
        major='size'
        minor='spacing'

    tots = tots.sort( major)

    means=tots.groupby([major, minor]).mean()
    stds=tots.groupby([major, minor]).std()
    counts=tots.groupby([major, minor]).count()

    # can't remember what/why all these reset_index are for. :-/
    su=stds.reset_index(drop=False) 
    cu=counts.reset_index(drop=False)
    
    jitter=0.0

    if scalex=='min':
        scalepix=pix_per_arcmin
    elif scalex=='mar':
        scalepix=pix_per_arcmin
    else:
        scalepix=1.0


    # TODO: Tabs/spaces in this section were very messed up,
    # may not work correctly (12/7/2018)
    for key, grp in means.reset_index(drop=False).groupby([major]):

        if key in ignore_sizes: continue

        unflanks = grp[grp.spacing==5000]
        if len(unflanks>0):
            last=len(grp[minor])-1
        else:
            last=len(grp[minor])

       	st= su[(su[major]==key)]
       	xvals=np.array( [float(aval) for aval in grp[minor] ])
        yvals=np.array( [float(aval) for aval in grp['cor'] ])
        yerr=np.array(  [float(aval) for aval in st['cor'] ])

        if scalex=='mar':
            scalepix=int(key)
            pix_per_arcmin=int(key)
            lbl='%s %s'%(lbl_prefix, pretty_pix( int(key), pix_per_arcmin))
            #lins=plt.errorbar( xvals+jitter, yvals, yerr=yerr, marker='o', label=lbl, ls=ls )
            xv=pixmin(xvals[0:last],scalepix)+jitter
        if scalex=='pix2':
            xv += 20-int(key)

        x_g1d = ndimage.gaussian_filter1d(pixmin(xvals,scalepix), smooth_sigma)
        y_g1d = ndimage.gaussian_filter1d(yvals, smooth_sigma)
        lo = lowess(yvals, pixmin(xvals,scalepix), frac=frac_lo )
        if norm=='unflanked':
            yvals /= y_g1d[-4]
        y_g1d = ndimage.gaussian_filter1d(yvals, smooth_sigma) # recompute

        if plot_diff:
       	    lins=plt.plot( xv , yvals[0:last], marker='o', label=lbl, ls='', alpha=0.8 )
       	    plt.plot( xv, yvals[0:last], marker='', ls=ls, alpha=0.2, color=lins[0].get_color())
	    #plt.fill_between( xv, yvals[0:last]-yerr[0:last], yvals[0:last]+yerr[0:last], color=lins[0].get_color(), alpha=0.1 )
        else:
       	    lins=plt.plot( xv , yvals[0:last], marker='o', label=lbl, ls='', alpha=0.8 )
       	    plt.plot( xv, yvals[0:last], marker='', ls=ls, alpha=0.2, color=lins[0].get_color())
	    #plt.fill_between( xv, yvals[0:last]-yerr[0:last], yvals[0:last]+yerr[0:last], color=lins[0].get_color(), alpha=0.1 )

	# Silly/not working way to do unflanked--above is better
        #unflanks = grp[grp.spacing==5000]
        if len(unflanks>0):
            print (key, xvals[-1], yvals[-1], yerr[-1])
            ax=plt.gca();
            if scalex=='min':
               lastloc=2
            elif scalex=='mar':
               lastloc=4
            else:
               lastloc=pixmin(xvals[-2],scalepix)+1.0+jitter

            ax.errorbar(lastloc, yvals[-1], yerr=yerr[-1], marker='s', ms=10, color=lins[0].get_color() ) #, label='unflanked' )

        else:
            lastloc=xvals[-1]
	
        if plot_smoothed:
            plt.plot(x_g1d,y_g1d, color=lins[0].get_color(), lw=2.0 )
            plt.plot(lo[:,0],lo[:,1], color=lins[0].get_color(), ls=':', lw=2.0 )
            jitter += jitter_inc

        xl=plt.xlim()
        try:
            if scalex=='min':
                plt.xlim(pixmin(xl[0])*0.9-0.1, lastloc*1.1 )
            elif scalex=='mar':
                plt.xlim(pixmin(xl[0])*0.9-0.1, lastloc*1.1 )
            else:
                plt.xlim(xl[0]*0.9-0.1, lastloc*1.1 )
        except:
            print ("no lastloc")
            xvals=np.array([0]) # to avoid error on return statement
            yvals=np.array([0]) # to avoid error on return statement

    if scalex=='min':
        plt.xlabel('Minutes', size=18)
    elif scalex=='mar':
        plt.xlabel('xMAR', size=18)
    else:
        plt.xlabel('pixels', size=18)

    plt.ylim(0, 1.1 )
    plt.ylabel( 'Prop. correct') 
    plt.legend( loc='best', prop={'size':8})
    plt.grid(True)

    if (pix_per_arcmin>0.0) and not (scalex=='min'):
        upperax( ax1, pix_per_arcmin )

    #if scalex==False:
    	#upperax(ax1, pix_per_arcmin)
    
    return tots,totc,pixmin(xvals,scalepix), yvals

def upperax(ax=None, pix_per_arcmin=1.0):
    def tick_function(X, pix_per_arcmin):
    	return [pretty_pix(x, pix_per_arcmin, add_decimal=False) for x in X]

    if ax==None:
    	ax=plt.gca()
    ax2=ax.twiny()
    tick_locs=ax.get_xticks()
    ax2.set_xticks(tick_locs)
    ax2.set_xticklabels(tick_function(tick_locs, pix_per_arcmin), rotation=45 )
    ax2.set_xlim( ax.get_xlim())

def upperax_pix(ax=None, pix_per_arcmin=1.0):
    def tick_function(X, pix_per_arcmin):
    	return [pretty_pix(x, pix_per_arcmin, add_decimal=False) for x in X]

    if ax==None:
    	ax=plt.gca()
    ax2=ax.twiny()
    tick_locs=ax.get_xticks()
    xr=np.arange(0,35,5)
    ax2.set_xticks(xr/pix_per_arcmin)
    #ax2.set_xticks(tick_locs,xr*pix_per_arcmin)
    #ax2.set_xticklabels([x*pix_per_arcmin for x in tick_locs], rotation=45 )
    ax2.set_xticklabels(xr, rotation=0 )
    ax2.set_xlim( ax.get_xlim())
    plt.grid(True)
