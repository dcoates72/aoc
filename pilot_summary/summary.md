<!--
Source file to generate PDF summary of preliminary pilot results.
Should live a directory beyond the figures that are output by the
IPython notebook.
-->

# Fig 1. DC pilot data: 2 letter sizes
### Shades are std. deviation of multiple (2-3) blocks--same for subsequent plots.
### All distances are edge-edges between outer edges of Tumbling-Es (0=touching)
![](../dc_aoc_pix.pdf)

\newpage

# Fig 2. RS pilot data (smaller pixels, better AO correction)
### (Less data/certainty at 7px and 10px conditions..)
![](../rs_aoc_pix.pdf)

\newpage

# Fig 3. Both subjects better/larger sizes, in minutes.
![](../both_min_easy.pdf)

### Observations:
1. Crowded critical spacing less than 1 minute of arc for high-contrast Tumbling-Es flanked by Tumbling-Es that are at AO-corrected acuity limit (<20/10 for Ram, 20/15 for Dan).
1. ...Around 0.6' for ceiling conditions. (DC and RS 11px)
1. Is the slight facilitation real for RS 10px (green) or RS 11px (red)? 
1. Are the 'ripples' real?

\newpage

# Fig 4. Both subjects smaller sizes, in minutes.
![](../both_min_hard.pdf)

1. Facilitation zone seems based on physical distance, approximately 0.3-0.4', even though letter sizes vary.
1. Then, each curve rises to an initial 'bump', drops, then rises again, etc.
1. Is this pattern real? Does it have a periodicity tied to either the physics of the stimulus or performance?
1. Is the slope of the rise tied to either the physics of the stimulus or performance? Can we relate to Dan and Dennis "canonical crowing curve" somehow?

\newpage

# Fig 5. Multiples of bar width on its own doesn't seem meaningful..
![](../both_mar.pdf)

1. My intuition is that some magic function could align the hills and valleys 

\newpage

## Major themes:
1. Facilitation
	1. Extent (0.3-0.4') ?
	1. Slope of drop (seemed different across subjects)
1. There is crowding greater than 1 bar-width
1. The extent of this crowding is not much... (<2')
1. The "bumps" are intriguing.. What is the meaning of their periodicity? Is it interesting? Something like foveal spurious resolution?

* Family of curves remind me of Takahashi's data (next page), with some
necessary horizontal shifting and smoothing.

\newpage

# Fig 5. Non-AO results

![](../dc_non.pdf)
![](../rs_non.pdf)
\newpage
![](takahashi-fig19-05deg.png)Takahashi
