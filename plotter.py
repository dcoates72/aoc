import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import statsmodels.api as sm
import arvofit
from scipy.interpolate import interp1d
import scipy

def ptoz(p):
    z=np.sqrt(2) * scipy.special.erfinv( 2*p-1.0)
    return z

def plotds(ds,nsiz=0,cm=plt.cm.spectral, newfig=True):

	if newfig:
		plt.figure( figsize=(6,6) )
	sizes=np.sort( ds.raw['size'].unique() )
	siz1=ds.raw[ ds.raw['size']==sizes[nsiz]]
	#print len(siz1)
	#print siz1
	siz1.loc[ siz1['spacing']>60, 'spacing'] = ds.ppm*2.0
	#siz1.loc[ I#, 'spacing']=siz1['spacing'] / ds.ppm
	#siz1['spacing']=siz1['spacing'] / ds.ppm
	siz1.loc[:,'spacing']=siz1['spacing'] / ds.ppm
	siz1.loc[:,'prop']= siz1['cor']/np.array(siz1['N'],dtype='float')

	spacs=siz1['spacing'].unique()
	fils=np.sort( siz1['filenum'].unique() )
	#for nspac,aspac in enumerate(spacs):
	#print fils
	#print fils

	print ds.who, ds.which

	y=siz1.prop
	x=siz1['spacing']
	lo=sm.nonparametric.lowess(y,x)
	f=interp1d( lo[:,0], lo[:,1], bounds_error=False )
	xnew=np.linspace(x.min(),x.max())
	ynew=f(xnew)
	plt.plot( xnew, ynew, color='g' )

	asy=ynew[-1]
	if asy>0.98:
		asy=0.98
	#siz1['zdiff']=ptoz(siz1['prop'] )-ptoz(asy)
	siz1.loc[:,'zdiff']=(siz1['prop'] ) #-(asy)

	for nfil,afil in enumerate(fils):
		num=np.float(nfil)/len(fils)
		colr=cm( num )
		filt=siz1[ siz1['filenum']==afil]
		#filt.loc[ filt['spacing']>60, 'spacing'] = ds.ppm*2.0
		#unspacs=filt['spacing']>60
		#filt[unspacs]['spacing'] = ds.ppm*3.0
		#print filt,afil
		#print spacs, ds.ppm
		plt.plot(filt['spacing']+num/100.0, filt['zdiff'], color=colr, ls='', marker='o', alpha=0.2, label=nfil )

	mens=siz1.groupby(['size','spacing']).mean().reset_index(drop=False)
	stds=siz1.groupby(['size','spacing']).std().reset_index(drop=False)['cor']

	plt.errorbar( mens['spacing'], mens['cor']/mens['N'], yerr=stds/mens['N'], marker='', ls='' )

	if newfig:
		plt.legend()
	#plt.ylim(-0.5,1.1)
	plt.grid()

	groups=siz1.groupby(['filenum','spacing']) #.reset_index(drop=False)

	return siz1

def plotall(ds):
	sizes=ds.raw["size"].unique()

	for nsize,asize in enumerate(sizes):
		plotds(ds,nsize, newfig=False)
