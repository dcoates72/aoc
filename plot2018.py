import numpy as np
import matplotlib.pyplot as plt
from newfit import trace_params, fix_params, unlo, param_ci, ci95
import finalfit
import finalplots
import analysis2018 as analysis

def safecat(arr,x):
    if len(arr)==0:
        return x
    else:
        return np.concatenate ([arr,x])

def all_summ_plot(ds_all,dat, asys,
    xaxis='size', lines=True, zscore=False, linetype='linear', plot_points=True, stars=True, plot_bars=True):
    
    did_sub=np.zeros(6)
    mens=np.zeros(finalfit.conds)
    for n in np.arange(finalfit.conds):
        who,whowhat,nsiz,siz,ads=finalfit.cond2xxx(ds_all,n)

        if zscore:
            ci=finalfit.ptoz_np( ci95( dat[n] ) )
        else:
            ci=ci95( dat[n] )
            
        if ads.which=='ao': # need color for error bar
            mfc=finalplots.colors_sub[whowhat]
        else:
            mfc='white'
        fc=mfc #'k'
        ec=finalplots.colors_sub[whowhat]

        asy1=ci95( asys[n])[0]

        if xaxis=='size':
            xpos=siz/ads.ppm+who/150.0
        elif xaxis=='asy':
            xpos=asy1
            
        if stars==False: # is (smoothed) CS plots
            marker='o'
            ms=6
        elif asy1>=0.8 and asy1<=0.92:
            marker='*'
            ms=12
        else:
            marker='o'
            ms=8
            
        if marker=='o' and did_sub[whowhat]==0:
            if ads.which=='ao':
                lbl='%s %s'%(ads.who, ads.which.upper() )
            else:
                lbl='%s'%(ads.who)

            did_sub[whowhat]=1
        else:
            lbl=""
            
        mens[n]=ci[0]
        
        if plot_bars:
            # Plot separately to give different alphas to bars and errorbars
            plt.errorbar(xpos, ci[0], yerr=[[ci[0]-ci[1],ci[2]-ci[0]]], marker='',
                     ls='', mec=ec,
                     ecolor=ec,
                     color=fc, alpha=0.5 )
        if plot_points:
            plt.errorbar(xpos, ci[0], marker=marker,
                     ls='', mec=ec,
                     ecolor=ec, ms=ms,
                     color=fc, alpha=1.0, label=lbl )
            
    # Clear per-condition aggregate
    xline={} # these have all 1000 boots
    yline={}
    xl_mean={} # these are just the median/mean (from ci95)
    yl_mean={}
    for n in np.arange(6):
        xline[n]=[]
        yline[n]=[]
        xl_mean[n]=[]
        yl_mean[n]=[]
        
    for n in np.arange(finalfit.conds):
        whowhat=finalfit.map_cond[n]
        who=finalfit.map_sub[n]
        ads=ds_all[whowhat]
        nsiz= n-finalfit.map_sub_first[whowhat]
        siz= int(ads.sizes[nsiz])

        if xaxis=='size':
            xpos=siz/ads.ppm+who/150.0
        elif xaxis=='asy':
            xpos=ci95( asys[n])[0]
            
        # Append this set of y's (CSes) and x's (sizes)
        xline[whowhat] = safecat( xline[whowhat],[xpos]*1000 )
        if zscore:
            yline[whowhat] = safecat( yline[whowhat], finalfit.ptoz_np( dat[n]) )
        else:
            yline[whowhat] = safecat( yline[whowhat], dat[n] )

        xl_mean[whowhat]=safecat( xl_mean[whowhat], [xpos] )
        yl_mean[whowhat]=safecat( yl_mean[whowhat], [mens[n]] ) # redundant, prob. could do above
        
    # Fit a line and plot
    for n in np.arange(6):
        x1=xline[n]
        y1=yline[n]
        xr=np.array([x1.min(),x1.max()])
        if n%2==0:
            ls='-'
        else:
            ls='--'
        
        if lines:
            if linetype=='linear':
                res=np.polyfit(x1,y1,1)
                plt.plot( xr, xr*res[0]+res[1], color=finalplots.colors_sub[n], lw=3, alpha=0.6, ls=ls)
                #print res
            elif linetype=='walk':

                y_pred=analysis.infer_smooth(np.array(yl_mean[n]),0.3)
                print yl_mean[n], y_pred
                #return y_pred
                plt.plot( xl_mean[n], y_pred, color=finalplots.colors_sub[n], lw=5, alpha=0.6, ls=ls)   
            elif linetype=='connect':
                plt.plot( xl_mean[n], yl_mean[n], color=finalplots.colors_sub[n], lw=5, alpha=0.6, ls=ls)   
            else:
                xr=np.linspace(x1.min(), x1.max() )
                KR = sm.nonparametric.KernelReg
                kr = KR( y1,x1,'c', bw=[0.05],reg_type='ll')
                y_pred,y_est=kr.fit(xr)
                plt.plot( xr, y_pred, color=finalplots.colors_sub[n], lw=5, alpha=0.6, ls=ls)
        
    plt.grid( True )
