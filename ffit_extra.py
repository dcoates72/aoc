from finalfit import priors

# For this variant, keep facilitation outside of the z transform.
# Basic cumulative Gaussian goes from 0 to 1, and is shifted down to -1 to 0 then scaled.
def cumul_fac3_t(X,guess,asymptote,width,pse, facilitation_asymp, facilitation_slope):
    facil=facilitation_asymp-X*facilitation_slope
    crowded=logistic(X,0.25,asymptote,width,pse) #,erf=tsr.erf, sqrt=tsr.sqrt)
    #crowded_transform=ztop( 2.0*(crowded-1.0) + ptoz(asymptote) )
    combined=tsr.maximum( facil,crowded )
    return combined

def cumul_fac3_np(X,guess,asymptote,width,pse, facilitation_asymp, facilitation_slope):
    facil=facilitation_asymp-X*facilitation_slope
    crowded=logistic(X,0.25,asymptote,width,pse)
    #crowded_transform=ztop_np( 2.0*(crowded-1.0) + ptoz_np(asymptote) )
    combined=np.max( (facil,crowded_transform), 0 )
    return combined

def cumul_fac_t(X,guess,asymptote,width,pse, facilitation_asymp, facilitation_slope):
    facil=facilitation_asymp-X*facilitation_slope
    crowded=cumul(X,guess,asymptote,width,pse,erf=tsr.erf, sqrt=tsr.sqrt)
    combined=tsr.maximum( facil,crowded )
    return combined

def cumul_fac_np(X,guess,asymptote,width,pse, facilitation_asymp, facilitation_slope):
    facil=facilitation_asymp-X*facilitation_slope
    crowded=cumul(X,guess,asymptote,width,pse) #,erf=scipy.special.erf, sqrt=np.sqrt):
    combined=np.max( (facil, crowded), 0)
    return combined

def fit_cumul(ads,size, priors=priors(), boots=1000):
  dat=ads.data_sizes[size]
  Xt=dat['spacing'].values/ads.ppm
  Yt=dat['cor'].values
  N=dat['N'].values

  #Xt[ dat['spacing']>60] = 3.1

  guess=0.25

  basic_model=pm.Model()
  with basic_model:
    asymptote=pm.Beta(**priors['asymptote'])
    width=pm.HalfNormal(**priors['width'])
    pse=pm.Normal(**priors['pse'] )
    facilitation_asymp=pm.Beta(**priors['facilitation_asymp'])
    facilitation_slope=pm.Beta(**priors['facilitation_slope'])

    pf=cumul_fac_t(Xt,guess,asymptote,width,pse,facilitation_asymp,facilitation_slope)
    
    Y_obs = pm.Binomial( 'Y_obs', p=pf, n=N, observed=Yt)
   
    map_estimate = pm.find_MAP(model=basic_model)

    try:
        ads.map_estimate[size]=arvofit.fix_params(map_estimate)
    except AttributeError:
        ads.map_estimate={}
        ads.map_estimate[size]=arvofit.fix_params(map_estimate)

    ads.priors=priors

    if boots>0:
        #with timeit():
            #step = pm.NUTS(model=basic_model)
        with timeit():
            traces = pm.sample(boots) #, step=step, start=map_estimate)
    
        ads.traces[size]=traces
    else:
        traces=[]

    return map_estimate,basic_model,(Xt,Yt,N),traces

def fit_cumul_aggr(ads,size, priors=priors(), boots=1000):
  # Fit to a cumul gaussian that is shifted down to the range -1:0
  dat=ads.data_sizes[size]
  Xt=dat['spacing'].values/ads.ppm
  Yt=dat['cor'].values
  N=dat['N'].values

  Xt[ dat['spacing']>60] = 3.1

  basic_model=pm.Model()
  with basic_model:
    asymptote=pm.Beta(**priors['asymptote'])
    width=pm.Normal(**priors['width_normal'])
    pse=pm.Normal(**priors['pse'] )
    #facilitation_asymp=pm.Beta(**priors['facilitation_asymp'])
    facilitation_asymp=pm.Normal(**priors['facilitation_asymp_normal'])
    fa=facilitation_asymp*asymptote
    #facilitation_asymp=asymptote*facilitation_asymp
    #facilitation_slope=pm.Beta(**priors['facilitation_slope'])
    facilitation_slope=pm.Normal(**priors['facilitation_slope_normal'])

    # No guess rate for the arbitrary reduction fit
    pf=cumul_fac2_t(Xt,0.0,asymptote,width,pse,fa,facilitation_slope)
    
    # Now this is done inside t2:
    #est=( ptoz(pf)*0.5+1+(1.0-asymptote) ) 
    #est=ztop( 2.0*(pf-1.0) + ptoz(asymptote) )

    #print est
    Y_obs = pm.Binomial( 'Y_obs', p=pf, n=N, observed=Yt)
   
    map_estimate = pm.find_MAP(model=basic_model)

    try:
        ads.map_estimate[size]=arvofit.fix_params(map_estimate)
    except AttributeError: # none exist yet
        ads.map_estimate={}
        ads.map_estimate[size]=arvofit.fix_params(map_estimate)

    ads.priors=priors

    if boots>0:
        with timeit():
            step = pm.NUTS(model=basic_model)
        with timeit():
            traces = pm.sample(boots, step=step, start=map_estimate)
    
    try:
        ads.traces[size]=traces
    except AttributeError: # none exist yet
        ads.traces={}
        ads.traces[size]=traces
    else:
        traces=[]

    return map_estimate,basic_model,(Xt,Yt,N),traces

def who2n(who):
    dic={'S0':1,'S1':2,'S2':2}
    return dic
