import numpy as np
import pymc3 as pm
from theano import shared
from pymc3.distributions.timeseries import GaussianRandomWalk

from scipy import optimize
import pymc3 as pm
import matplotlib.pyplot as plt
import finalplots
import newfit
from finalfit import timeit
import finalfit
import statsmodels.api as smodels
#import somdels.nonparametric as nonparametric
from finalfit import ptoz_np as ptoz

LARGE_NUMBER = 1e5
def infer_smooth(y,smoothing):
    model = pm.Model()
    with model:
        smoothing_param = shared(0.9)
        mu = pm.Normal("mu", sd=LARGE_NUMBER)
        tau = pm.Exponential("tau", 1.0/LARGE_NUMBER)
        z = GaussianRandomWalk("z",
                               mu=mu,
                               tau=tau / (1.0 - smoothing_param),
                               shape=y.shape)
        #yhat = pm.Binomial('y', n=10, p=mu, observed=y)
        obs = pm.Normal("obs",
                        mu=z,
                        tau=tau / smoothing_param,
                        observed=y)

    # infer_z from http://docs.pymc.io/notebooks/GP-smoothing.html
    with model:
        smoothing_param.set_value(smoothing)
        res = pm.find_MAP(vars=[z], fmin=optimize.fmin_l_bfgs_b)
        return res['z']

def get_size_sets(ds,unflanked_sentinel=40):
    data={}
    for asize in np.sort( np.unique( ds.raw['size'] )):
        subset=ds.raw
        subset=subset[subset['size']==asize]
        subset.loc[ subset['spacing']==5000,'spacing']=unflanked_sentinel
        means=subset.groupby(['size','spacing'])['cor'].mean().reset_index()
        stds=subset.groupby(['size','spacing'])['cor'].std().reset_index()
        data[asize]=(subset,means,stds) #0=rawdata
    return data

def infer_prop(subset):
    trace={}
    for aspac in np.sort( subset['spacing'].unique() ):
        print aspac
        Y=subset[subset['spacing']==aspac].cor
        with pm.Model() as model:
            mu = pm.Uniform('mu', 0, 1)
            y = pm.Binomial('y', n=10, p=mu, observed=Y)
            trace[aspac] = pm.sample(1000)            
    return trace

def spacstats(trac):
    spacs=np.sort( trac.keys() )
    vals=np.array( [newfit.ci95(trac[aspac]['mu'])[0] for (nspac,aspac) in enumerate(spacs)] )
    return spacs,vals

def mytracsamp(trac):
    spacs=trac.keys()
    idxs=np.random.randint(1000,size=[len(spacs)])    
    vals=np.array( [trac[aspac][idxs[nspac]]['mu'] for (nspac,aspac) in enumerate(spacs)] )
    return spacs,vals

#  Demo: Fig for 2018 paper # TODO: Should be in plot2018
# Demo2: All6 demo
def plot1(ads, use_ppm=True, violins=True, bars=False, plot_smoothed=True, sm=0.5, sizes=[], demo=False, newfig=True, demo2=False, colors=[], smooth2=False, ylower=0.0, doz=False, asy1=[]):
    if newfig:
        plt.figure( figsize=(10,8))
    if sizes==[]:
        sizes=np.arange(len(ads.sizes))

    for nsize,asize in enumerate(sizes):
        if colors==[]:
            colr=finalplots.colors_size[asize]
        else:
            colr=colors[nsize]
        
        asize=ads.sizes[asize]
        #print asize

        size1=ads.data_size[ asize]
        sizes_trace=ads.infer_size[ asize ]
        if plot_smoothed:
            smoothed=infer_smooth(size1[1],sm)

        if demo2:
            if ads.which=='ao':
                # Titles in first row only
                plt.title("%s"%(ads.who), size=18 )
        else:
            plt.title("%s %s"%(ads.who, ads.which) )

        #x=asize[1]['spacing']
        sizes=np.sort( np.array([at for at in sizes_trace]) )

        if smooth2:
            KR = smodels.nonparametric.KernelReg
            kr = KR( size1[1],sizes,'c',bw=[0.25], reg_type='ll')
            y_pred,y_est=kr.fit(sizes) # actually spacings???

        if use_ppm:
            xdisp=sizes/ads.ppm+float(asize)/5000.0 # no jitter
        else:
            xdisp=sizes

        if violins:
            parts=plt.violinplot( [sizes_trace[at]['mu'] for at in sizes],
                   positions=xdisp,
                   showextrema=False, showmedians=False, widths=0.025*np.max(xdisp) );
                    # Scale the width with the maxX of the chart (pixls vs. mins)

            for pc in parts['bodies']:
                pc.set_facecolor(colr)
                #pc.set_edgecolor('k')
                pc.set_alpha(0.1)

        if demo:
            x_unflanked = 1.725
        elif demo2:
            x_unflanked=2.01
        elif doz:
            x_unflanked=2.02
        else:
            x_unflanked=2.02

        if bars:
            if True:
                cis=np.array( [ newfit.ci95(sizes_trace[at]['mu']) for at in sizes ]   )
                mid=cis[:,0]
                lower=mid-cis[:,1]
                upper=cis[:,2]-mid
    
                if demo:
                    plt.errorbar(xdisp[:-1], mid[:-1], yerr=[lower[:-1],upper[:-1]], color='gray', ls='', marker='', alpha=0.4 )
                    plt.errorbar([x_unflanked], mid[-1:], yerr=[[lower[-1]],[upper[-1]]], color='gray', ls='', marker='', alpha=0.4 )
                else:
                    plt.errorbar(xdisp, mid, yerr=[lower,upper], color=colr, ls='', marker='.', alpha=0.4 )
            else:
                plt.errorbar(xdisp, size1[1]['cor']/10.0, yerr=size1[2]['cor']/10.0, color=colr, ls='', marker='.', alpha=0.4 )
    
        #[np.mean(sizes_trace[wow]['mu']) for wow in sizes_trace]

        # Demo= first representative figure for 2018 paper
        if plot_smoothed:
            if demo:
                plt.plot(xdisp[0:8], smoothed[0:8,2]/10., color=colr, lw=5.0, alpha=0.6, label="%0.2f'"%(int(asize)/ads.ppm))
                plt.plot(xdisp[7:15], smoothed[7:15,2]/10., color='r', lw=5.0, alpha=0.6, label="%0.2f'"%(int(asize)/ads.ppm))
                plt.plot(xdisp[14:], smoothed[14:,2]/10., color='b', lw=5.0, alpha=0.6, label="%0.2f'"%(int(asize)/ads.ppm))
            else:
                plt.plot(xdisp[:], smoothed[:,2]/10., color=colr, lw=5.0, alpha=0.6, label="%0.2f'"%(int(asize)/ads.ppm))

        if demo:
            plt.plot(xdisp[:-1], np.array(size1[1]['cor'])[:-1]/10., color='grey', marker='o', ls='', label='', mec='k')
            plt.plot(x_unflanked, np.array(size1[1]['cor'])[-1]/10., color='grey', marker='s', ls='', label='', mec='k')
        elif demo2:
            plt.plot(xdisp[:-1], size1[1]['cor'][:-1]/10., color=colr, marker='o', ls='', label='', mec='k', ms=4.5)
            jit=np.random.uniform()/15.0-1/30.0
            plt.plot(x_unflanked+jit, np.array(size1[1]['cor'])[-1]/10., color=colr, marker='s', ls='', label='', mec='k')
            plt.errorbar(x_unflanked+jit, np.array(size1[1]['cor'])[-1]/10.0, yerr=np.array(size1[2]['cor'])[-1]/10.0, color=colr, ls='', marker='', alpha=0.6 )
        elif doz:
            yvals=size1[1]['cor']/10.
            yvals=ptoz(size1[1]['cor']/10.)-ptoz(asy1[nsize])  #np.array(size1[1]['cor'])[-1]/10.)
            plt.plot(xdisp[:-1], yvals[:-1], color=colr, marker='o', ls='', label='', mec='k', ms=4.6, alpha=1.0)
            plt.plot(x_unflanked, ptoz(np.array(size1[1]['cor'])[-1]/10.)-ptoz(asy1[nsize]),marker='s', ls='', label='', mec='k', ms=4.6, alpha=1.0)
        else:
            plt.plot(xdisp[:-1], size1[1]['cor'][:-1]/10., color=colr, marker='o', ls='', label='', mec='k', ms=4.5)
            plt.plot(x_unflanked, np.array(size1[1]['cor'])[-1]/10., color=colr, marker='s', ls='', label='', mec='k')

    plt.grid(True)
    plt.legend(loc='lower right', prop={'size':14} )

    if demo:
        plt.text(1.705, ylower, "//", ha='center', va='center', size=18 )
        plt.xlim(-0.02,1.7499)
        plt.xticks( plt.xticks()[0][:-1] )
        plt.xlim(-0.02,1.7499)
    elif demo2:
        plt.text(1.9, ylower, "//", ha='center', va='center', size=18 )
        #plt.xlim(-0.02,2.2)
        plt.xticks( plt.xticks()[0][:-2] )
        plt.xlim(-0.05,2.2)

def myezcomp(ress, doplot=True):
    with timeit():
        comp_df=pm.compare( map(lambda x:x[-1], ress ), map(lambda x:x[1], ress ) )
    print comp_df
    if doplot:
        pm.compareplot( comp_df )
    return comp_df

def pfstats(modl,xr=np.linspace(0,3,100), thresh=0.025, yokes=['none','none','none'],boots=1000,xform_z=False):
    # Regenerate PFs 
    sets=finalfit.conds
    yokemaps=finalfit.get_yokemaps(yokes,finalfit.conds)
    
    pfs=np.zeros( (finalfit.conds,boots,len(xr)) )
    for aset in np.arange(sets):
        key_width='width%d'%yokemaps[0][aset]
        key_pse='pse%d'%yokemaps[1][aset]
        key_fs='facilitation_slope%d'%yokemaps[2][aset]
        key_fa='facilitation_asymp%d'%aset
        key_asy='asymptote%d'%aset
        
        for fit1 in np.arange(boots):
            width=modl[-1][fit1][key_width]
            pse=modl[-1][fit1][key_pse]
            fs=modl[-1][fit1][key_fs]
            fa=modl[-1][fit1][key_fa]
            asy=modl[-1][fit1][key_asy]
    
            # This must match finalfit!!
            pf=finalfit.cumul_fac2_np(xr,0.0,asy*6.0-3.0,width*5,pse*2.0-1.0,fa*6.0-3.0,fs,xform_z=xform_z)
            pfs[aset,fit1]=0.25+pf*0.75
            #plt.plot( xr, pf)
    
    # Compute CSes, asyptotes, fa's, etc.
    cses=np.zeros( (finalfit.conds,boots))
    asys=pfs[:,:,-1]
    fas=pfs[:,:,0]
    peaks=np.zeros( (finalfit.conds) )
    for con in np.arange(finalfit.conds):
        cses[con]=np.array( [finalfit.cs2018( xr, pfs[con,n,:], thresh=thresh ) for n in np.arange(boots)] )
        peaks[con] = finalfit.peak2018( cses[con] )

    return cses,pfs,asys,fas,peaks,xr
