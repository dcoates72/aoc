from pymc3 import traceplot
from pymc3 import *
from pymc3.math import switch,maximum
import pymc3 as pm
import process_data
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import newfit
import util
import numpy as np

from newfit import fix_params, trace_params, ci95

from theano import config
#config.device = 'gpu0'
#config.floatX = 'float32'
#config.optimizer = 'fast_run'

def ztop(z):
    p=0.5 * (1.0+pm.math.erf(z/pm.math.sqrt(2) ) )
    return p

def ptoz(p):
    z=pm.math.sqrt(2) * pm.math.erfinv( 2*p-1.0)
    return z

def fitwork(Xt,Yt,guess=0.25,N=10,asymptote_alpha=1.0,asymptote_beta=1.0,width_sd=5.0,width_mu=0.5,pse1_sd=100.0,
            pse_mu=1.0, facilitation_slope_mu=1.0, facilitation_slope_sd=10.0, fac_sd=0.4, facilitation_asymptote_alpha=1.0,facilitation_asymptote_beta=1.0):

    basic_model=Model()
    with basic_model:
        asymptote=Beta('asymptote', alpha=asymptote_alpha, beta=asymptote_beta)
        width=Normal('width', mu=width_mu, sd=width_sd)
        pse=Normal('pse', mu=pse_mu, sd=pse1_sd)  
        facilitation_asymp=Beta('facilitation_asymp', alpha=facilitation_asymptote_alpha, beta=facilitation_asymptote_beta) 
        facilitation_slope=HalfNormal('facilitation_slope', sd=facilitation_slope_sd)

        pf=logistF(Xt,asymptote,width,pse,facilitation_asymp, facilitation_slope)
        Y_obs = Binomial( 'Y_obs', p=pf, n=N, observed=Yt*N)
        map_estimate = find_MAP(model=basic_model)

    return map_estimate,basic_model


def fit1(x_tofit, y_tofit, N=10, doplot=False, dotrace=False, dotraceplot=False, traceN=1000, priors=None):
    #map_estimate,model=fitwork( x_tofit, y_tofit, pse_mu=0.35, pse1_sd=0.1,
    map_estimate,model=fitwork( x_tofit, y_tofit, N=N, **priors )

    if dotrace:
        with model:
            trace = sample(traceN, start=map_estimate)
            if dotraceplot:
                traceplot(trace)
    else:
        trace=[]

    return model,map_estimate,trace

def ram1size(data, size,traceN=1000,dotraceplot=False):
    return do1size( data, size, process_data.ram_pix_per_arcmin1, traceN, dotraceplot)

def do1size(data, size,ppm,traceN=1000,dotraceplot=False, priors={}):
    asize=filterdat( data, 'size', size)
    x_tofit=np.array(asize.spacing,dtype='float')/ppm
    y_tofit=asize.cor
    try:
        N=np.array(asize.N)
    except AttributeError:
        N=30 # if there is no count information, just use some default
    model,map_estimate,trace=fit1( x_tofit, y_tofit, N=N,dotrace=True, dotraceplot=dotraceplot, traceN=traceN, priors=priors )
    return model,map_estimate,trace

def pfit_good( map_estimate, thedata, sizes, raw_data, ppm ):
    plt.figure( figsize=(5,4) )
    for nsiz,asiz in enumerate(thedata):

        colr=cm.spectral_r(float(nsiz)/len(thedata))
        
        raw=filterdat( raw_data[0], 'size', sizes[nsiz])
        groups=newfit.grouper(raw, 'spacing', 'cor' )
        plt.errorbar( groups[0][0:-1]/ppm, groups[1][0:-1], #yerr=groups[2][0:-1],
                 ls='', marker='o', color=colr, alpha=0.25 )
    
        plt.errorbar( groups[0][-2]/ppm+0.5, groups[1][-1], yerr=groups[2][-1],
                 ls='', marker='s', color=colr, alpha=0.25 )
  
        l95,lval=log95(**fix_params( trace_params( asiz[2], map_estimate) ))
        plt.plot( [l95, l95], [lval,1.1], color=colr, ls='--' )

        xr=np.linspace(0,groups[0][-2]/ppm+0.6,100)
        
        params=fix_params( trace_params( asiz[2], map_estimate) )
        params['X']=xr
        params['guess']=0.25
        y=logistF_np(**params)

        lbl="bar=%0.3f\'"%( float(sizes[nsiz])/ppm )
        plt.plot( xr, y, '-', label=lbl, color=colr, lw=1.5 )
        
        plt.xlim(-0.1,groups[0][-2]/ppm+0.5+0.1)
    
    plt.legend( loc='best', prop={'size':8})
    plt.xlabel("Edge-edge spacing (min)", size=16)
    plt.ylabel("Prop. correct", size=16)
    plt.grid( False )

def find_facilitation_last(asymptote,width,pse,facilitation_asymp,facilitation_slope,xr_limits=[0,1.0],guess=0.25,xr_len=500):
    xr=np.linspace(xr_limits[0], xr_limits[1], xr_len)
    facil=facilitation_asymp-xr*facilitation_slope
    curve=logist(xr,guess,asymptote,width,pse)
    try:
        valley_idx=np.arange(xr_len)[facil<=curve][0]
    except IndexError:
        valley_idx=0
    return np.array(xr[valley_idx],facil[valley_idx])

def logist(X,guess,asymptote,width,pse):
    pf=guess+(asymptote-guess)/(1.0+np.exp(-width*(X-pse)))
    return pf

def logistF_np(X,asymptote,width,pse,facilitation_asymp,facilitation_slope,guess=0.25):
    facil=facilitation_asymp-X*facilitation_slope
    combined=np.max( (facil, logist(X,guess,asymptote,width,pse)), 0 )
    return combined

def logistF(X,asymptote,width,pse,facilitation_asymp,facilitation_slope,guess=0.25):
    facil=facilitation_asymp-X*facilitation_slope
    combined=maximum( facil, logist(X,guess,asymptote,width,pse) )
    return combined

def logistF2(X,asymptote,width,pse,facilitation_asymp,facilitation_slope,guess=0.25):
    facil=facilitation_asymp-X*facilitation_slope
    combined=maximum( facil, logist(X,guess,1.0,width,pse) )
    combined=( ztop( ptoz(( combined ) - asymptote ) ) )
    return combined

def texas_logistFN(X,asymptote,width,pse,facilitation_asymp,facilitation_slope,guess=0.25):
    facil=facilitation_asymp-X*facilitation_slope 
    logi=logist(X,guess,1.0,width,pse)
    combined=maximum( facil, logi )
    return (combined)

def texas_logistFN_np(X,asymptote,width,pse,facilitation_asymp,facilitation_slope,guess=0.25):
    facil=facilitation_asymp-X*facilitation_slope
    logi=logist(X,guess,1.0,width,pse)
    combined=np.max( (facil, logi), 0)
    return (combined)

def logistFN(X,asymptote,width,pse,facilitation_asymp,facilitation_slope,guess=0.25):
    facil=facilitation_asymp-X*facilitation_slope 
    logi=logist(X,guess,1.0,width,pse)
    combined=maximum( facil, logi )
    return (combined*1.5-1.5)

def logistFN_np(X,asymptote,width,pse,facilitation_asymp,facilitation_slope,guess=0.25):
    facil=facilitation_asymp-X*facilitation_slope
    logi=logist(X,guess,1.0,width,pse)
    combined=np.max( (facil, logi), 0)
    return (combined*1.5-1.5)

def loge5(asymptote,width,pse,level=0.95,facilitation_asymp=0,facilitation_slope=0,guess=0.25):
    right_side=1./level-1
    #print right_side
    x=-np.log(right_side)/width+pse
    y=logistF_np(x,guess,asymptote, width, pse,facilitation_asymp, facilitation_slope )
    return x,y

def filterdat(dat,field,value):
    return dat[dat[field]==value]

def find_cses(traces):
    for asiz in traces:
        tra=asiz[2]
        cses=np.array([log95(**fix_params(atrace))[0] for atrace in tra ])
        maxes=np.array([log95(**fix_params(atrace))[1] for atrace in tra ])
        print (ci95(cses), ci95(maxes))
    return ci95(cses), ci95(maxes), cses, maxes

def all_cses(traces):
    cses= [ [log95(**fix_params(atrace))[0] for atrace in asiz[2] ] for asiz in traces]
    maxes=[ [log95(**fix_params(atrace))[1] for atrace in asiz[2] ] for asiz in traces]
    return cses, maxes

def cond_summary_plot( results, raw_data, ppm, save_filename=""):
    import util
    reload(util)
    sizes = raw_data[0]['size'].unique()
    s_int = np.array( raw_data[0]['size'].unique(), dtype="int" )
    map_estimate=results[0][1]
    pfit_good( map_estimate, results, sizes, raw_data, ppm )
    util.upperax_pix(pix_per_arcmin=ppm)
    if save_filename!="":
        plt.savefig("plots/%s-pfs.pdf"%save_filename, bbox_inches="tight")
        plt.savefig("plots/%s-pfs.png"%save_filename, bbox_inches="tight")

    cs_all=all_cses(results)
    plt.figure( figsize=(8,3))
    for cs_param in [1,0]:
        plt.subplot(1,2,cs_param+1)
        plt.violinplot( cs_all[:][cs_param], np.array(sizes,dtype='int'), showextrema=False  )

        cis=[ci95(asiz[:])[0] for asiz in cs_all[cs_param]]
        cis_l=[ci95(asiz[:])[1] for asiz in cs_all[cs_param]]
        cis_u=[ci95(asiz[:])[2] for asiz in cs_all[cs_param]]
    plt.plot(s_int, cis, marker='o', ls='-' )
    for nsiz,asiz in enumerate(s_int):
        xl=s_int[nsiz]+0.1
        yl=cis[nsiz]+0.05
        plt.text(xl,yl, "%0.2f"%cis[nsiz] )

        plt.xticks( np.array(sizes,dtype='int') )
        if plt.ylim()[1]>10:
            plt.ylim( -1, 10)

    return cis,cis_l,cis_u,cs_all

def autosizes(data, ppm, dotraceplot=False, traceN=1000, subfix="", extra={}):
    sizes = data[0]['size'].unique()
    if subfix=="rs_non":
        extra['pse_mu']=10.0
    return [do1size( data[0], s, ppm, traceN=traceN, dotraceplot=dotraceplot) for s in sizes ]

def auto_trace_stats_texas( trace, raw=None, whichtrace=0,whichstat=0 ): #0=med,1=lower,2=upper
    #sizes = raw[0]['size'].unique()
    param_names=fix_params(trace[0]) # just to remove the _log_ dudes
    stats={}
    for aparam in param_names:
        stats[aparam]=ci95(trace[:][aparam])
    return stats


def auto_trace_stats( traces, raw=None, whichtrace=0,whichstat=0 ): #0=med,1=lower,2=upper
    #sizes = raw[0]['size'].unique()
    param_names=fix_params(traces[0][2][0]) # just to remove the _log_ dudes
    stats={}
    for aparam in param_names:
        stats[aparam]=ci95(traces[whichtrace][2][:][aparam])[whichstat]
    return stats

def auto_trace_stats_simple( traces, raw=None, whichtrace=0,whichstat=0 ): #0=med,1=lower,2=upper
    #param_names=fix_params(traces) # just to remove the _log_ dudes
    stats={}
    medians={}
    for nparam,aparam in enumerate(fix_params(traces[0])):
        stats[aparam]=ci95(traces[aparam])
        medians[aparam]=stats[aparam][0]
    return stats,medians

def auto_trace_t( traces): # just transpose, do anything
    #sizes = raw[0]['size'].unique()
    param_names=fix_params(traces[0]) # just to remove the _log_ dudes
    stats={}
    for nparam,aparam in enumerate(param_names):
        stats[aparam]=traces[:][aparam]
    return stats

def auto_trace_violin( traces, raw ):
    sizes = raw[0]['size'].unique()
    param_names=fix_params(traces[0][2][0]) # just to remove the _log_ dudes
    for aparam in param_names:
        plt.figure(figsize=(3,3) )
        plt.title( aparam )
        vals_per_size=[abag[2][:][aparam] for abag in traces]
        plt.violinplot( vals_per_size, np.array(sizes,dtype='int') )
        plt.xticks( np.array(sizes,dtype='int') )

def auto_trace_grid( traces, raw, save_filename='pf_est' ):
    sizes = np.array(raw[0]['size'].unique(), dtype='int' )
    param_names=fix_params(traces[0][2][0]) # just to remove the _log_ dudes
    plt.figure( figsize=(15,3) )

    bests={}
    for nparam,aparam in enumerate(param_names):
            plt.subplot(1,5,nparam+1)
            plt.title( aparam )
            means=np.array( [ fix_params( trace_params( asiz[2], param_names,0) )[aparam] for asiz in traces ] )
            lowers=np.array( [ fix_params( trace_params( asiz[2], param_names,1) )[aparam] for asiz in traces ] )
            uppers=np.array( [ fix_params( trace_params( asiz[2], param_names,2) )[aparam] for asiz in traces ] )

            fudge=0.0
            if aparam=='asymptote':
                fudge=0.25

                boots=[asiz[2][aparam]+fudge for asiz in traces]
                plt.violinplot( boots, sizes, showextrema=False )

                plt.plot( sizes, means+fudge, marker='o', color='b')
                plt.fill_between( sizes, lowers+fudge, uppers+fudge, color='b', alpha=0.1)

            for nsiz,asiz in enumerate(sizes):
                xl=asiz+0.1
                yl=(means+fudge)[nsiz]+0.05
                plt.text(xl,yl, "%0.2f"%(means+fudge)[nsiz] )
        
                if aparam=='pse':
                    plt.ylim( -1, 1)
            
                plt.xticks(sizes)
                plt.xlim(sizes[0]-0.25, sizes[-1]+0.25)
            bests[aparam]=means+fudge
        
    plt.subplot(1,5,3)
    plt.xlabel("Bar size (pix)", size=18)

    plt.savefig("plots/%s.pdf"%save_filename, bbox_inches='tight')
    plt.savefig("plots/%s.png"%save_filename, bbox_inches='tight')
    return bests

def cumul(X,guess,asymptote,width,pse,erf):
    # HACK: To fit the cumul for acuity, take the log of the abscissa, which yields equivalent slopes for different sizes
    #pf=guess+(1.0-guess-asymptote)*0.5*(1.0+erf( ( np.log10(X)-pse)/(width*np.sqrt(2)) )  )

    pf=guess+(1.0-guess-asymptote)*0.5*(1.0+erf( ( X-pse)/(width*np.sqrt(2)) )  )
    return pf

def cumul_old(X,guess,asymptote,width,pse,erf):
    pf=guess+(1.0-guess-asymptote)*0.5*(1.0+erf( ( X-pse)/width*np.sqrt(2) )  )
    return pf

def fit_cumul(Xt,Yt,guess=0.25,alpha=1.0,beta=100.0,width_sd=50.0,width_mu=10.0,pse1_sd=0.5,
#def fit_cumul(Xt,Yt,guess=0.25,alpha=1.0,beta=2.0,width_sd=5,width_mu=2,pse1_sd=1.0,
            pse_mu=None, N=100 ):
  from theano.tensor import erf
  pse_mu = np.mean(Xt)
  basic_model=Model()
  #print alpha,beta,width_sd,pse1_sd,pse_mu
  #print zip(Xt,Yt)
  with basic_model:
    asymptote=Beta('asymptote', alpha=alpha, beta=beta)
    #width=Normal('width', mu=width_mu, sd=width_sd)
    width=Beta('width', alpha=width_mu, beta=width_sd)
    pse=Normal('pse', mu=pse_mu, sd=pse1_sd)  

    pf=cumul(Xt,guess,asymptote,width,pse,erf)
    
    Y_obs = Binomial( 'Y_obs', p=pf, n=N, observed=Yt*N)
   
    map_estimate = find_MAP(model=basic_model)
    return map_estimate,basic_model

def fit_acuity1(x_tofit, y_tofit, doplot=False, dotrace=False, dotraceplot=False, traceN=1000):
    #map_estimate,model=fitwork( x_tofit, y_tofit, pse_mu=0.35, pse1_sd=0.1,
    map_estimate,model=fit_cumul( x_tofit, y_tofit)

    if dotrace:
        with model:
            trace = sample(traceN, start=map_estimate)
            if dotraceplot:
                traceplot(trace)
    else:
        trace=[]

    return model,map_estimate,trace

