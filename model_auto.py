import matplotlib.pyplot as plt

import numpy as np
import os.path as path #to combine paths together safe for Linux/Windows/Mac..
import statsmodels.api as sm
lowess = sm.nonparametric.lowess

import util
import process_data
import pandas as pd
#import arvofit
import data_new
#import plotter
import finalfit
import finalplots
#import newfit

import scipy

import arvofit
import pymc3 as pm
from arvofit import logistF
import pymc3.math

# Convenience functions to time execution (and display start time)
class timeit():
    from datetime import datetime
    def __enter__(self):
        self.tic = self.datetime.now()
        print('Start: {}').format(self.tic)
    def __exit__(self, *args, **kwargs):
        print('Runtime: {}'.format(self.datetime.now() - self.tic))


def auto1(n=0, boots=1000, yokes=['subs','subs','all'], scaled_fa=True):
    if True:
        # DC 01/15/2018: Pickling seems to fail! (Pickle from ipynb). Regen.
        ds_all=data_new.load_all_oo()
    elif False:
        ds_all=np.load("ds_all2018.npy") #("dsset_may2017.npy")
        print "pickle w/fits"
    else:
        ds_all=data_new.load_all_oo()
        print "just data"

    res=finalfit.fit_cumul_all( ds_all, boots=boots, yokes=yokes, scaled_fa=scaled_fa, xform_z=False)
    print "RES: ",n,pm.dic( res[-1],res[1]),pm.waic( res[-1], res[1] ),boots,str(yokes),scaled_fa
    np.save("res%03d"%n,res)

if __name__ == "__main__":
    import sys
    if len(sys.argv)<2:
        print "USAGE: python XXX.py start stride howmany"
    else:
        nstart=int( sys.argv[1] )
        nstride=int( sys.argv[2] )
        nhowmany=int( sys.argv[3] )
        yokes=sys.argv[4].split(',')

    #print yokes
    #print len(yokes)
    #sys.exit()
    for n in (nstart+np.arange(nhowmany)*nstride):
        print "BOOTNUM:",n
        auto1(n=n, boots=1000, yokes=yokes, scaled_fa=False) #, scaled_fa=True, yoke='subs_shuffle' )
