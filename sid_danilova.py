import matplotlib.pyplot as plt
import matplotlib.ticker
import numpy as np

import shared as shared

xhc=np.array([
[0.493506,47.2602],
[0.987013,35.6743],
[2,56.3462],
[2.98701,75.8666],
[5.01299,87.9795],
[9.97403,87.8871]
])
xmc=np.array([
[0.311688,40.3546],
[0.571429,40.7143],
[1.19481,50.6543],
[1.79221,65.2123],
[2.98701,83.5589],
[9.97403,87.8871]
])
xlc=np.array([
[0.181818,47.6748],
[0.38961,56.8856],
[0.805195,61.0764],
[1.19481,67.962],
[2,81.7308],
[9.97403,87.8871],
])

xh = xhc[:,0]
xm = xmc[:,0]
xl = xlc[:,0]

yh = xhc[:,1]
ym = xmc[:,1]
yl = xlc[:,1]

hsize = 10**-0.1
msize = 10**0.1
lsize = 10**0.3

sizes = np.array([hsize, msize, lsize])

def amult(size): return np.array([1,size])
def scaler(base,size): return base*np.array([size,1])

nom_ee = np.array([xhc, xmc, xlc])
abs_ee = np.array([scaler(xhc,hsize), scaler(xmc,msize), scaler(xlc,lsize)])

def fit3cons_old(data, skipfirst=True):
    pees = np.zeros( (len(data),3) ) # 3=#params in cumgauss fit
    for n,condata in enumerate(data):
        if n<2 and skipfirst:
            thisdatx = condata[1:,0]
            thisdaty = condata[1:,1]
        else:
            thisdatx = condata[:,0]
            thisdaty = condata[:,1]

        # At HC, don't use closest point, since it shows facilitation
        afit = shared.cumgauss_func(thisdatx, thisdaty)
        afit.inits[1] = 0.7
        afit.fit2()
        if n<2 and skipfirst:
            afit.x = condata[:,0]
            afit.y = condata[:,1]
        afit.plot( False, label='%0.3g %0.3g %0.3g' % (afit.p[0], afit.p[1], afit.r2()) )
        pees[n] = afit.p

    return pees


names = np.array(['hc', 'mc', 'lc'])
strokes_letter = 5
cc2ee = 0.5
def plotarr(arr, logx=False, legend=True):
    for conn,con in enumerate(arr):
        plt.plot( con[:,0], con[:,1], '.-', label=names[conn] )

    if logx:
        #plt.loglog()
        ax=plt.gca()
        #ax.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
        ax.set_xscale('log')
        ax.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())

    if legend:
        plt.legend(loc='best')

def make_abs(arr):
    arr2=arr.copy()
    for conn,con in enumerate(arr2):
        con[:,0] *= sizes[conn]
    return arr2

def make_cc(arr):
    arr2=arr.copy()
    for conn,con in enumerate(arr2):
        con[:,0] += strokes_letter * cc2ee
    return arr2

def outarr(arr, filename='out.csv'):
    f= open(filename,'wt')
    for subn,sub in enumerate(arr):
        f.write('%s\n' % ['hc', 'mc', 'lc'][subn])
        for row in sub:
            for cell in row:
                f.write(str(cell) + '\t')
            f.write('\n')
    f.close()

def sid4():
    plt.figure()
    plt.subplot(2,2,1)
    plotarr( nom_ee)
    plt.xlim(0,10)
    plt.ylabel('Edge to edge')
    plt.title('nominal')

    plt.subplot(2,2,2 )
    plotarr( make_abs(nom_ee), logx=True )
    plt.xlim(0.3,4.5)
    plt.title('absolute')

    plt.subplot(2,2,3)
    plotarr( make_cc(nom_ee))
    plt.xlim(0,10)
    plt.xlabel('Strokes')
    plt.ylabel('Center to center')

    plt.subplot(2,2,4)
    plotarr( make_abs( make_cc(nom_ee)) )
    plt.xlabel('Minutes')
    plt.show()

def trans_nop(x):
    return x

def fit3cons(data, skipfirst=True, logx=False):
    pees = np.zeros( (len(data),3) ) # 3=#params in cumgauss fit
    if logx:
        trans = np.log10
    else:
        trans = trans_nop
    fits = [shared.cumgauss_func(trans(condata[:,0]),condata[:,1]) for condata in data]
    for n,condata in enumerate(data):
        if n<2 and skipfirst:
            first = 1
        else:
            first = 0
        thisdatx = condata[first:,0]
        thisdaty = condata[first:,1]

        afit = fits[n]
        afit.x = trans(thisdatx)
        afit.y = thisdaty
        afit.inits[0] = np.mean(afit.x)
        afit.inits[1] = 0.5
        afit.fit2()
        afit.x = trans(condata[0:,0]) # show all the data
        afit.y = condata[0:,1]
        afit.plot( False, logx=logx, label='(%0.3g,%0.3g) %0.3g' % (afit.p[0], afit.p[1], afit.r2()) )
        pees[n] = afit.p

    return pees,fits

abs_cc=make_abs( make_cc(nom_ee))

# Why the strange order?
# js
bond0x=np.array([2.1,2.5,3.1]) 
#bond0y=np.array([1.3,1.0, 0.9])
bond0y=np.array([1.84,1.74, 1.5])
bond0s=np.array([0.11,0.12,0.15])
bond0c=np.array([0.72,0.92,0.952])

# km
bond1x=np.array([2.2, 2.5, 2.7, 4.7])
#bond1y=np.array([2.2, 2.2, 1.9, 1.4])
bond1y=np.array([3.35, 3.2, 2.9, 1.88])
bond1s=np.array([0.11,0.12,0.13,0.23])
bond1c=np.array([0.37,0.65,0.835,0.94])

#ib
bond2x=np.array([2.1,2.7,4.7])
#bond2y=np.array([1.9, 1.9, 0.9])
bond2y=np.array([2.75, 2.4, 1.4])
bond2s=np.array([0.11,0.13,0.23])
bond2c=np.array([0.55,0.86,0.96])

#vb
bond3x=np.array([2.25,2.7])
bond3y=np.array([2.2,2.05])
bond3s=np.array([0.11,0.11])
bond3c=np.array([0.63,0.74])

#md
bond4x=np.array([3.15, 3.6])
bond4y=np.array([2.7,2.5])
bond4s=np.array([0.11,0.11])
bond4c=np.array([0.8775,0.916])

bond=np.array([[bond0x,bond0y,bond0s,bond0c],
[bond1x,bond1y,bond1s,bond1c],
[bond2x,bond2y,bond2s,bond2c],
[bond3x,bond3y,bond3s,bond3c],
[bond4x,bond4y,bond4s,bond4c]])

labels = ['JS', 'KM', 'IB', 'VB', 'MD']

markers=['^','<','>','v','x']
def bond_cool(alldata, cc=False, norm2mar=False, m=1.0, color_tran=lambda x:x): # what proportion of the letter size to multiply by
    xd=np.array([data[0] for data in alldata])
    if cc:
        yd=np.array([data[1]+data[0]*m for data in alldata])
    else:
        yd=np.array([data[1] for data in alldata])
    mars = 1
    if norm2mar:
        mars = np.array([sub[1] for sub in alldata[:,0]])/5.0
        yd = yd/mars
    #return mars, yd
    #lines = [plt.errorbar(xd[n], yd[n] , yerr=data[2], marker='', label=labels[n]) for n,data in enumerate(alldata)]


    for n,data1 in enumerate(alldata):

        if n==4: continue # skip nd.. they are consistent, but under our data points
    
        m,b=np.polyfit(xd[n],yd[n],1)
        #print m,b
        colr=plt.cm.spectral( color_tran(alldata[n,3]) ) # asympt. prop corr. cubed.
        #lines=plt.plot( xd[n], xd[n]*m+b, '-', label='%d %s'%(n,labels[n]), color=colr, marker=markers[n], ms=10 )

        if n==0:
            lbl='Danilova&Bondarko 2007'
        else:
            lbl=''
        lines=plt.plot( xd[n], xd[n]*m+b, '--', color='gray', alpha=0.1, lw=2, label=lbl )

        plt.scatter( xd[n], yd[n], c=colr, marker=markers[n], s=40, label='%s'%(labels[n]) )

    #c=[aline[0].get_color() for n,aline in enumerate(lines)])
        #plt.scatter( xd[n], yd[n], c=lines.get_color(),s=4**xd[n] )  #c=[aline[0].get_color() for n,aline in enumerate(lines)])

    if cc: yl='center-center'
    else: yl='edge-edge'
    if norm2mar:
        yl += '(strokes)'
    else:
        yl += '(arcmin)'

    plt.ylabel(yl)
    plt.xlabel('Landolt C size (arcmin)')
    plt.title('Danilova/Bondarko 2006 Exp 1')
    #plt.show()

def bond2(alldata=bond, color_tran=lambda x:x):
    plt.figure( figsize=(11,8.5) )
    plt.subplot(1,2,1)
    bond_cool(alldata, False, color_tran)
    plt.legend(loc='best')
    plt.subplot(1,2,2)
    bond_cool(alldata, True, color_tran)
    plt.show()
    

flom_mar = np.array([0.5, 0.55, 0.55, 0.6, 0.61, 0.65, 0.71])
flom_x = np.array([1.9,2.7,2.6,3.1, 2.3, 3.2, 3.8] )
