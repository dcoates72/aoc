import numpy as np
import process_data
import matplotlib.pyplot as plt

def p2m_rs(data):
    return np.log( data/process_data.ram_pix_per_arcmin1 ) #/5.0*60.0

def unlo(x): return np.exp(x)/(1+np.exp(x))

def grouper( data, fields_group, field_value ):
    groups=data.groupby(fields_group, sort=True)
    means=np.array(groups.mean()[field_value] )
    stds=np.array( groups.std()[field_value] )
    keys=np.array( np.sort( groups.groups.keys() ), dtype='int' )
    return keys,means,stds

def pfit(datx,daty,initial,doplot=True, dotrace=False, dotraceplot=False, tracenum=500):   
    xr=np.linspace(-3,1,50)
    hat,modl=dofit( p2m_rs(datx), daty, initial=initial )
    #ypf=logist(xr, 0.25, unlo(hat['asymptote1_logodds_']), np.exp(hat['width1_log_']), hat['pse1'] )
    ypf=logist(xr, 0.25, unlo(hat['asymptote1_logodds_']), hat['width1'], hat['pse1'] )

    if doplot:
    	plt.plot( xr, ypf )
    	plt.plot( p2m_rs(datx), daty, 'o', alpha=0.2)

    if dotrace:
        from pymc3 import traceplot
        with modl:
            trace = sample(tracenum,start=hat)

        if dotraceplot:
        	traceplot(trace)
        else:
            trace=[]
        
    return hat,modl,trace

#pfit( res_both_non[0], res_both_non[1], [-2] )
#hat,modl,trace=pfit( res_both[0], res_both[1], [-1] )
#hat,modl=dofit( p2m(res_both_non[0]), res_both_non[1], initial=[-2] )
#ypf=logist(xr, 0.25, unlo(hat['asymptote1_logodds_']), np.exp(hat['width1_log_']), hat['pse1'] )
#plt.plot( xr, ypf )

# This big Cell has all the fitting/plotting code

def logist(X,guess,asymptote,width,pse):
    pf=guess+(1-guess-asymptote)/(1.0+np.exp(-width*(X-pse)))
    return pf

# Initial: mu of pse1
def dofit(Xt,Yt,guess=0.25, initial=[5]):
    from pymc3 import Model, Normal, HalfNormal, Uniform, DiscreteUniform, Beta, sample, Binomial
    from pymc3.math import switch
    from pymc3 import find_MAP

    basic_model=Model(verbose=2)

    with basic_model:
        #asymptote1=Uniform('asymptote1', alpha=2, beta=1)
        asymptote1=Beta('asymptote1', alpha=1.0, beta=1.0) # uniform on [0,1]
        width1=Normal('width1', mu=5, sd=5.0)
        pse1=Normal('pse1', mu=initial[0], sd=10.)
    
        pf=logist(Xt,guess,asymptote1,width1,pse1)
        #print initial[0], Xt,Yt, pf
        Y_obs = Binomial( 'Y_obs', p=pf, n=10, observed=Yt*10)

    map_estimate = find_MAP(model=basic_model)
    print(map_estimate)
    return map_estimate,basic_model

# Convert 95% confidence interval values (mean, -95*, +95)] to a value
# that is suitable for use in yerror errorbars
def ci2eb( triple ):
	return np.array(triple[1]-triple[0], triple[2]-triple[0] )

def ci95( arra, pval=0.05):
    vals=np.sort(arra)
    return np.array( [vals[int( len(vals)/2.0 )], vals[int(pval/2.0*len(vals))], vals[int( (1-pval/2.0)*len(vals) )] ] )
        
def param_ci( trace, param, pval=0.05):
    return ci95(trace[param])

def fix_params(p_orig):
    params=p_orig.copy()
    for aparam in p_orig:
        if aparam[-9:]=='_logodds_':
            params[ aparam[0:-9] ] = unlo(p_orig[aparam])
            del params[aparam]
        elif aparam[-10:]=='_logodds__': # DC new this started to break at some point
            params[ aparam[0:-10] ] = unlo(p_orig[aparam])
            del params[aparam]
        elif aparam[-5:]=='_log_':
            params[ aparam[0:-5] ] = np.exp(p_orig[aparam])
            del params[aparam]
        elif aparam[-6:]=='_log__':
            params[ aparam[0:-6] ] = np.exp(p_orig[aparam])
            del params[aparam]
    return params

def trace_params(trace,p_ref,which_stat=0):
    params=p_ref.copy()
    for aparam in p_ref:
        params[ aparam ] = param_ci( trace, aparam )[which_stat]
    return params
