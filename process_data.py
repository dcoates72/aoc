import numpy as np
import os.path as path
import util

dc_pix_per_arcmin1=8.24147 #150mm
ram_pix_per_arcmin1=16.48294 * 1.15 #  300mm

def load_all():
    RSdir0=path.join('Subjects','RS','results_8112016_spacing-8-9-10-11')
    RSdir1=path.join('Subjects','RS','results_8052016_spacing7-8-9-11')
    
    files2plt0 = [0,1,2,3,4,5]
    files2plt1 = [0,1,4,5]

    files_ram=[]

    for i in files2plt0:
        files_ram.append(path.join(RSdir0,'RS_ee_flanked_spacing_ao_08112016-0%s.csv'%str(i)))
    
    for j in files2plt1:
        files_ram.append(path.join(RSdir1,'RS_ee_flanked_spacing_ao_08052016-0%s.csv'%str(j)))

    # Note: the following uses string class join(), not os.path.join()
    fnamesStr_ram = ' '.join(files_ram)

    ignore_sizes=[]
    
    #ao_ram=util.dolist( files_ram, pix_per_arcmin=ram_pix_per_arcmin1, figsize=(12,10), ignore_sizes=[], jitter_inc=0.0,
                        #scalex='min', smooth_sigma=1.1, norm='unflanked');

    rs_non_files=[path.join("results","results_10112016_nonao_spacings_12-13-15",
                           "RS_ee_flanked_spacing_nonao_10112016-%02d.csv"%num) for num in [2,3,4,5] ]

    #nonao_ram=util.dolist( rs_non_files, lbl_prefix="RS-N", ignore_sizes=[], pix_per_arcmin=ram_pix_per_arcmin1, scalex='min', jitter_inc=0.0 );  


    # TODO: those aren't using load_raw yet, so don't use this
    ao_ram=[]
    nonao_ram=[]
    
    dc_ao1_dir= path.join('Subjects','DC','results_freshdilation2')
    dc_ao1_files=[ path.join(dc_ao1_dir,'drc_ee_ao_sanity_07252016-02.csv'),
              path.join(dc_ao1_dir,'drc_ee_ao_spacings_07252016-00.csv'),
              path.join(dc_ao1_dir,'drc_ee_ao_spacings_07252016-01.csv'),
             ]
    dc_ao2_dir= path.join('Subjects','DC','results_beforelunch')
    dc_ao2_files=[ path.join(dc_ao2_dir,"drc_ee_ao_spacings_07262016-00.csv"),
               path.join(dc_ao2_dir,"drc_ee_ao_spacings_07262016-01.csv") ]
    ao_dc=util.dolist_raw( dc_ao2_files+dc_ao1_files )#, pix_per_arcmin=dc_pix_per_arcmin1, figsize=(12,10), lbl_prefix='DC', jitter_inc=0.0, scalex="min" );

    dc_non2_dir=path.join('Subjects','DC','results_freshdilation2')
    dc_non3_dir=path.join('Subjects','DC','results_beforelunch')
    dc_non2_files=[path.join(dc_non2_dir,f) for f in [ r"drc_ee_non_spacings_07252016-00.csv",r"drc_ee_non_spacings_07252016-01.csv", r"drc_ee_non_spacings_07252016-02.csv" ] ]
    dc_non3_files=[path.join(dc_non3_dir,f) for f in [ r"drc_ee_non_spacings_07262016-00.csv",r"drc_ee_non_spacings_07262016-01.csv", r"drc_ee_non_spacings_07262016-02.csv"  ] ]
    dc_data_non=util.dolist_raw( dc_non3_files+dc_non2_files) #),  lbl_prefix="DC-N",fig=None, ignore_sizes=[], pix_per_arcmin=dc_pix_per_arcmin1, scalex='pix', jitter_inc=0.0 );

    return [ao_dc, dc_data_non, ao_ram, nonao_ram]

def get_raw( data, siz, facilitation_skip_index=0, unflanked_sentinel=2000, unflanked_newval=32 ):
        data=data[0][ data[0]['size']=='%02d'%siz ]
        #X,Y=np.array(data['spacing']),np.array(data['cor'])
        X,Y=(data['spacing']),(data['cor'])
    
        noFacil=(X>=facilitation_skip_index)
        Xt=X[noFacil]
        Yt=Y[noFacil]
        unfl=(Xt>=unflanked_sentinel)
        Xt[unfl]=unflanked_newval

        spac_groups=data.groupby('spacing', sort=True)
        means=np.array(spac_groups.mean() ).T[0]
        stds=np.array( spac_groups.std() ).T[0]
        spacs=np.sort( spac_groups.groups.keys() ) # TODO: God, i hope this is the right way to do this! Ugh

        return Xt, Yt, [spacs,means,stds]
