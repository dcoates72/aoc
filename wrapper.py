import makelet
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pixlet
import importlib

#abuf = makelet.make_let(0, 5, 128, 0, 0, 5, 10)
#plt.imshow( abuf, interpolation='Nearest', cmap=cm.bone)
#plt.show()
params=importlib.import_module('params')
apixlet=pixlet.PixStim(params=params)
apixlet.update()
pixlet.bufshow(apixlet.bits)