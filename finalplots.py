import matplotlib.pyplot as plt
import numpy as np
import arvofit
from finalfit import ptoz_np, ztop_np, cumul_fac2_np
from ffit_extra import cumul_fac_np # may be unnecessary
import scipy
import scipy.stats as st
from newfit import ci95
import statsmodels.api as sm
import pandas as pd # for density plot

# spectral order
cmaps_sizes=["Reds", "Oranges",  "Greens", "Blues", "Purples"]
colors_size=['r','orange', 'g', 'b', 'purple', 'gray']

colors_sub=['r','r', 'green', 'green', 'purple', 'purple']
ls_sub=['-',':', '-', ':', '-', ':']

def my_contour(x,y, ymin=0.2,ymax=1.1,xmin=-0.1,xmax=1.7,steps=100j,
    cmap='Blues', alpha=0.6 # for filled contours
    ):

    # Peform the kernel density estimate
    xx, yy = np.mgrid[xmin:xmax:steps, ymin:ymax:steps]
    positions = np.vstack([xx.ravel(), yy.ravel()])
    values = np.vstack([x, y])
    kernel = st.gaussian_kde(values)
    f = np.reshape(kernel(positions).T, xx.shape)

    fig = plt.gcf()
    ax = fig.gca()
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)

    # Contourf plot
    #cfset = ax.contourf(xx, yy, f, cmap=cmap)
    ## Or kernel density estimate plot instead of the contourf plot
    #ax.imshow(np.rot90(f), cmap='Blues', extent=[xmin, xmax, ymin, ymax])
    # Contour plot'
    colr=plt.get_cmap(cmap)(0.9)
    cset = ax.contour(xx, yy, f, colors='k', alpha=0.2)

    #cfset = ax.contourf(xx, yy, f, levels=np.concatenate[ ([0,cset.levels[3:]])], cmap=cmap)
    cfset = ax.contourf(xx, yy, f, levels=cset.levels[3:], cmap=cmap, alpha=alpha)


def valley_contour(x,y, ads, siz, dat=None, xr=None, pf=None, ymin=0.2,ymax=1.1,xmin=-0.1,xmax=1.7,steps=100j, dots=True,
    cmap='Blues', alpha=0.6 # for filled contours
    ):

    # Peform the kernel density estimate
    xx, yy = np.mgrid[xmin:xmax:steps, ymin:ymax:steps]
    positions = np.vstack([xx.ravel(), yy.ravel()])
    values = np.vstack([x, y])
    kernel = st.gaussian_kde(values)
    f = np.reshape(kernel(positions).T, xx.shape)

    fig = plt.gcf()
    ax = fig.gca()
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)

    # Contourf plot
    #cfset = ax.contourf(xx, yy, f, cmap=cmap)
    ## Or kernel density estimate plot instead of the contourf plot
    #ax.imshow(np.rot90(f), cmap='Blues', extent=[xmin, xmax, ymin, ymax])
    # Contour plot'
    colr=plt.get_cmap(cmap)(0.9)
    cset = ax.contour(xx, yy, f, colors='k', alpha=0.2)

    #cfset = ax.contourf(xx, yy, f, levels=np.concatenate[ ([0,cset.levels[3:]])], cmap=cmap)
    cfset = ax.contourf(xx, yy, f, levels=cset.levels[0:], cmap=cmap, alpha=alpha)

    # Label plot
    ax.clabel(cset, inline=1, fontsize=10)
    ax.set_xlabel('Y1')
    ax.set_ylabel('Y0')

    if dots and not(dat==None):
        plt.plot( dat[0], dat[1]/np.array(dat[2], dtype='float'), 'o', alpha=0.2 )
    if not xr==None:
        plt.plot( xr, pf)

    # Plot the MAP
    minval,minloc=valley1(arvofit.fix_params(ads.map_estimate[siz]) )
    plt.plot( minloc, minval, 'x', color=colr, ms=20 )

    #xlim(-0.1,3.1)
    #plt.show()
    return cset

from scipy.interpolate import UnivariateSpline

def safepdiff(v1,v2, fudge_thresh=1.99, fudge_mult=0.995, noz=False):

    # Correct for guess rate
    v1 = v1*0.75 + 0.25
    v2 = v2*0.75 + 0.25

    if noz==False:
        if len(v1)>1:
            v1[v1>fudge_thresh]*=fudge_mult


        if v2>fudge_thresh:
            v2 *= fudge_mult

        res=ptoz_np(v1)-ptoz_np(v2)
    else:
        res=v1-v2
    #res[(v1==1.0)&(v2==1.0)]=0.0
    return res

def plot_points(ads,size, color, alpha=0.2, div=1.0, xform="", loess=False, asy=None, cs_thresh=-0.05, xval_unflanked=2.1):
    dat=ads.data_sizes[size]
    #Xt=dat['spacing'].values/ads.ppm/div
    means=dat.groupby(['spacing']).mean()['cor']
    stds=dat.groupby(['spacing']).std()['cor']
    spacs=dat.groupby(['spacing']).mean().reset_index()['spacing']/ads.ppm*div
    Yt=dat['cor'].values
    spacs[ spacs>50 ] = xval_unflanked
    N=np.array(dat['N'].values,dtype='float')
    #plt.plot( Xt, Yt/N, marker='o', ls='', color=color, alpha=alpha )
    x=spacs
    y=np.array( means/10.0 )
    yerr=stds/10.0

    xr=np.linspace(0,xval_unflanked,200)

    x2=np.copy(x)
    #x2[x2==3.1]=2.3
    lowess = sm.nonparametric.lowess
    #print y[-1], x2[-1]
    z = lowess(y, x2, frac=0.3) #, frac= 1./3, it=0)

    KR = sm.nonparametric.KernelReg
    kr = KR( y,x2,'c',bw=[0.25], reg_type='ll')
    y_pred,y_est=kr.fit(xr)
    cs=0
    if not (xform==""):
        try:
            if asy==None:
                asy=ci95( ads.traces[size]['asymptote'] )[0]
                #mapa=ads.map_estimate[size]["asymptote"]
        except:
            if asy==None:
                asy=y[-1]

        #print "XX: ",  asy, y_pred[-1]
        #asy = asy #y_pred[-1] 
        #print asy
        #yval=ptoz_np(y_pred)-ptoz_np(asy)
        if xform=="zdiff":
            yval=safepdiff(y_pred,asy,noz=False)
        elif xform=="diff":
            yval=safepdiff(y_pred,asy,noz=True)
        #cs=np.arange(len(xr))[yval>=cs_thresh][0]
    else:
        asy=y_pred[-1]
        yval=y_pred


    if loess:
        plt.plot( xr, yval, color=color, ls='--' )

    if xform=="zdiff":
        #plt.errorbar( x, ptoz_np(asy)-ptoz_np(y), yerr=yerr, marker='o', ls='', color=color, alpha=alpha, label='' )
        #plt.errorbar( x, ptoz_np(y)-ptoz_np(asy), yerr=yerr, marker='o', ls='', color=color, alpha=alpha, label='' )
        plt.errorbar( x, safepdiff(y,asy,noz=False), yerr=yerr, marker='o', ls='', color=color, alpha=alpha, label='' )
    elif xform=="diff":
        plt.errorbar( x, safepdiff(y,asy,noz=True), yerr=yerr, marker='o', ls='', color=color, alpha=alpha, label='' )
    else:
        plt.errorbar( x, y, yerr=yerr, marker='o', ls='', color=color, alpha=alpha, label='' )

        #plt.plot( x, z, color=color )

        #spl=UnivariateSpline( x,y, k=4 )
        #spl.set_smoothing_factor(0.5)
        #plt.plot( xr, spl(xr), lw=3 )
    return [x,y],[xr,yval],cs,asy


def plot1fit(parms,xr=np.linspace(0,3),color='k', alpha=0.1, xform=False, doplot=True,ls='-',marker=''):
    parms=arvofit.fix_params(parms)

    if xform:
        try:
            pf=cumul_fac2_np( xr,0.0, parms['asymptote'], parms['width'], parms['pse'],
                parms['asymptote']*parms['facilitation_asymp'],
                parms['facilitation_slope'])
        except KeyError:
            pf=cumul_fac2_np( xr,0.0, parms['asymptote'], parms['width'], parms['pse'],
                parms['facilitation_asymptote'],
                parms['facilitation_slope'])
    else:
        #guess=0.25
        #pf=cumul_fac_np(xr,guess, parms['asymptote'], parms['width'], parms['pse'],
            #parms['facilitation_asymp'],
            #parms['facilitation_slope'])
        pf=cumul_fac2_np( xr,0.0, parms['asymptote'], parms['width'], parms['pse'],
                parms['asymptote']*parms['facilitation_asymp'],
                parms['facilitation_slope'])

    #if xform:
        #pf=(ptoz_np(pf)-ptoz_np(pf[-1])) * 0.5 + 1
    if doplot:
        #if pf[-1]==1.0:
            #pf[-1]=0.9999
        if xform:
            # TODO: I don't think this works, generally
            plt.plot( xr, (2*pf-1.0), color=color, alpha=alpha, ls=ls, marker=marker )
        else:
            plt.plot( xr, pf, color=color, alpha=alpha, ls=ls, marker=marker )
        

    return pf


def contours2(p1,p2,ds_all, subplots=True):
    plt.figure(figsize=(12,10))
    ax=plt.gca()

    xmin_t=100
    xmax_t=-100
    ymin_t=100
    ymax_t=-100

    for n in np.arange(6):
        if subplots:
            ax=plt.subplot( 2,3,n+1)
            
        ads=ds_all[n]
        
        for nsiz,siz in enumerate(ads.sizes):
            atrace=ads.traces[siz]
            
            map_estimate=arvofit.fix_params(ads.map_estimate[siz])
            #pd.DataFrame(atrace["width"]).plot.density(ax=ax, color=colors_size[nsiz], label=siz)
            #pd.DataFrame(1.0-atrace["asymptote"]).plot.density(ax=ax, color=colors_size[nsiz], label=siz)
            xmin=atrace[p1].min()
            xmax=atrace[p1].max()
            ymin=atrace[p2].min()
            ymax=atrace[p2].max()
            
            my_contour(atrace[p1], atrace[p2],cmap=cmaps_sizes[nsiz],
                                xmin=xmin, xmax=xmax,
                                ymin=ymin, ymax=ymax )

            xmin_t=min( (xmin,xmin_t))
            xmax_t=max( (xmax,xmax_t))
            ymin_t=min( (ymin,ymin_t))
            ymax_t=max( (ymax,ymax_t))
            
            stats_w=ci95( atrace[p1] )
            stats_a=ci95( atrace[p2] )
            plt.plot( stats_w[0], stats_a[0], 'x', ms=12, color=colors_size[nsiz])
            
            plt.plot( map_estimate[p1], map_estimate[p2], '*',
                     ms=12, color='w', mec=colors_size[nsiz], label='%d MAP'%int(siz)  )

            try:
                p1s=np.concatenate( ([stats_w[0]], p1s ))
                p2s=np.concatenate( ([stats_a[0]], p2s ))
            except UnboundLocalError:
                p1s=np.array( [stats_w[0] ])
                p2s=np.array( [stats_a[0] ])

       # plt.xlim(0.0,3.0)
        plt.legend( loc='best')
        plt.xlabel(p1)
        plt.ylabel(p2)

    if subplots:
        for n in np.arange(6):
            ax=plt.subplot( 2,3,n+1)
            plt.xlim( xmin_t, xmax_t)
            plt.ylim( ymin_t, ymax_t)
    else:
        plt.xlim( xmin_t, xmax_t)
        plt.ylim( ymin_t, ymax_t)

    return p1s,p2s

def plot1pf(ads, xr, xform=False, dofits=True, loess=False, sizes=None):
    try:
        if sizes==None:
            sizes=ads.sizes
    except ValueError:
        # Ok, can't test array against len
        pass
    for nsize, asize,color in zip( np.arange(len(sizes)), sizes, colors_size[0:len(sizes)] ):
        colr=colors_size[nsize]
           
        #abscissa=(xr-ads.map_estimate[asize]["pse"])*ads.map_estimate[asize]["width"]
        abscissa=xr #*ads.ppm/float(asize) #/(float(asize)/ads.ppm*5.0)
        
        if dofits:

            modes={}
            for aparam in ads.traces[asize][0]:
                stats=ci95( ads.traces[asize][aparam] )
                modes[aparam]=stats[0]
            pf_mode=plot1fit( modes, xr=xr, xform=xform, doplot=False)
            if xform=="zdiff":
                pf2=ptoz_np(pf_mode)-ptoz_np(pf_mode[-1])
            elif xform=="diff":
                pf2=pf_mode-pf_mode[-1]
            else:
                pf2=pf_mode
            lin=plt.plot( abscissa, pf2, ls='-', color=colr )                 
            
            # Use fits for shade and center
            fits=[plot1fit( aset, xr=xr, xform=xform, doplot=False) for aset in ads.traces[asize] ]
            sums=np.apply_along_axis( ci95, 0, fits )
            if xform:
                noz=(xform=="diff")
                yvals=safepdiff(( np.mean(fits,0) ), ( pf_mode[-1]), noz=noz )
                lower=safepdiff(( sums[1] ), ( pf_mode[-1]), noz=noz )
                upper=safepdiff(( sums[2] ), ( pf_mode[-1]), noz=noz )
            else:
                yvals=np.mean(fits,0)
                lower=sums[1]
                upper=sums[2]
            lbl='Bar=%0.2f\''% ( float(asize)/ads.ppm )
            lin=plt.plot( abscissa, yvals, label=lbl, color=colr, ls='--' )      
            plt.fill_between( abscissa, lower, upper,color=colr, alpha=0.18)
           
        # points:
        res=plot_points( ads, asize, color=colr, loess=loess, xform=xform ) #div=1+0*ads.ppm/float(asize)) #float(asize)/ads.ppm*5.0)

        # map estimate
        #xmiddle=ads.map_estimate[asize]["pse"]
        #ymiddle=finalplots.plot1fit( ads.map_estimate[asize], xr=np.array([xmiddle]),alpha=0.02, color=colr,
                              #      xform=False, doplot=False)
        #plt.plot( xmiddle, ymiddle, 'o',color=colr  )
        
        pf_map=plot1fit( ads.map_estimate[asize], xr=xr,alpha=0.02, color=colr,
                                   xform=True, doplot=False)

        #ordinate=#
        #ordinate=pf_map
        #ordinate=ptoz_np(pf_map) - ptoz_np(pf_map[-1]) 

        #plt.plot( abscissa , ordinate, ls='--', color=colr)
    plt.grid(True)
    plt.legend(loc='lower right' )
    #plt.semilogx()
    #plt.xlim(0,0.5)
    
    plt.xlabel("Edge-to-edge flanker spacing (arcmin)", size=18)
    if xform:
        plt.ylim(-2.0,0.5)
        plt.ylabel("Crowding reduction (Z-scores)", size=18)
    else:
        plt.ylim(0.18,1.1)
        plt.ylabel("Proportion correct", size=18)
    #plt.ylim(0.21,1.01)
    return res

def quick_density( arr, reuse=True, color=None ):
    if reuse:
        ax=plt.gca()
        pd.DataFrame(arr).plot(kind='density', ax=ax, color=color)
    else:
        pd.DataFrame(arr).plot(kind='density')
