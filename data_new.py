import pandas as pd
import os.path as path
import util
import numpy as np
import process_data

pt_ao_files1=[ 'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01122017-00.csv',
                      'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01122017-01.csv',
                     'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01122017-02.csv',
                     'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01132017-00.csv',
                     'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01132017-01.csv',
                     'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01132017-02.csv',
                     'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01132017-03.csv',
                     'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01132017-04.csv']

data_pt_ao1=util.dolist_raw(pt_ao_files1)

#data_pt_ao2=util.dolist([ 'Subjects/PT/ao2/PT_ee_flanked_spacing_ao_02032017-%02d.csv'%n for n in [0,1,2,3,5,6] ])
#ds_pt_ao2=dataset.dataset("pt", "ao",process_data.ram_pix_per_arcmin1,np.array([10,11,12]), data_pt_ao2 )
#ds_pt_ao2.dofit(2000)
#dataset.doparams(ds_pt_ao2)

#data_pt_all= pd.concat( [data_pt_ao, data_pt_ao2])
data_pt_all=util.dolist_raw([ 'Subjects/PT/ao2/PT_ee_flanked_spacing_ao_02032017-%02d.csv'%n for n in [0,1,2,3,5,6] ]
                + ['Subjects/PT/ao/PT_ee_flanked_spacing_ao_01122017-00.csv',
                      'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01122017-01.csv',
             'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01122017-02.csv', 'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01132017-00.csv',
                     'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01132017-01.csv', 'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01132017-02.csv',
                      'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01132017-03.csv',
                     'Subjects/PT/ao/PT_ee_flanked_spacing_ao_01132017-04.csv'] )

#ds_pt_aoA=dataset.dataset("pt", "ao",process_data.ram_pix_per_arcmin1,np.array([10,11,12,13]), data_pt_all )

if True:
    RSdir0=path.join('Subjects','RS','results_8112016_spacing-8-9-10-11')
    RSdir1=path.join('Subjects','RS','results_8052016_spacing7-8-9-11')
    
    files2plt0 = [0,1,2,3,4,5]
    files2plt1 = [0,1,4,5]

    files_ram=[]

    for i in files2plt0:
        files_ram.append(path.join(RSdir0,'RS_ee_flanked_spacing_ao_08112016-0%s.csv'%str(i)))
    
    for j in files2plt1:
        files_ram.append(path.join(RSdir1,'RS_ee_flanked_spacing_ao_08052016-0%s.csv'%str(j)))

    # Note: the following uses string class join(), not os.path.join()
    fnamesStr_ram = ' '.join(files_ram)

    ignore_sizes=[]
    
ao_ram=util.dolist_raw( files_ram )

data_pt_non =util.dolist_raw([ 'Subjects/PT/nonao/PT_ee_flanked_spacing_nonao_01182017-%02d.csv'%n for n in np.arange(0,7)])
##data_pt_non1=util.dolist_raw([ 'Subjects/PT/nonao/PT_ee_flanked_spacing_nonao_01182017-%02d.csv'%n for n in np.arange(0,5)])
##data_pt_non2=util.dolist_raw([ 'Subjects/PT/nonao/PT_ee_flanked_spacing_nonao_01182017-%02d.csv'%n for n in np.arange(5,7)])

rs_non_files=[path.join("results","results_10112016_nonao_spacings_12-13-15",
    "RS_ee_flanked_spacing_nonao_10112016-%02d.csv"%num) for num in [2,3,4,5] ]
data_rs_non=util.dolist_raw(rs_non_files)

[data_s1_ao,data_s1_non,_,_]=process_data.load_all()

def load_all_old():
    # Old version, pre-OO

    dset_s1_ao =do1data(data_new.data_s1_ao,'S1','ao',process_data.dc_pix_per_arcmin1)
    dset_s1_non=do1data(data_new.data_s1_non,'S1','non',process_data.dc_pix_per_arcmin1)
    dset_s2_ao =do1data(data_new.ao_ram,'S2','ao',process_data.ram_pix_per_arcmin1)
    dset_s2_non=do1data(data_new.data_rs_non,'S2','non',process_data.ram_pix_per_arcmin1)
    dset_s3_ao =do1data(data_new.data_pt_all,'S3','ao',process_data.ram_pix_per_arcmin1)
    dset_s3_non=do1data(data_new.data_pt_non,'S3','non',process_data.ram_pix_per_arcmin1)
    
    ds_all=[dset_s1_ao, dset_s1_non, dset_s2_ao, dset_s2_non, dset_s3_ao, dset_s3_non]
    
    for ads in ds_all:
        data_trials=ads[1]
        data_trials['flip']=( (data_trials["resp"]=='f') & (data_trials["targ"]=='e') ) | ( (data_trials["resp"]=='e') & (data_trials["targ"]=='f')
                        ) | ( (data_trials["resp"]=='3') & (data_trials["targ"]=='E') ) | ( (data_trials["resp"]=='E') & (data_trials["targ"]=='3') )

    return ds_all


### New OO

class data_new():
    def __init__(self, data, who, which, ppm):
        self.raw=data[2]
        self.data_trials=data[1]
        self.who=who
        self.which=which
        self.ppm=ppm
        data_all={}
        self.sizes=self.raw['size'].unique()
        for asize in self.sizes:
            data_all[asize]=self.raw[self.raw['size']==asize]
        self.data_sizes=data_all
    
def load_all_oo():
    dset_s1_ao =data_new(data_s1_ao,'S1','ao',process_data.dc_pix_per_arcmin1)
    dset_s1_non=data_new(data_s1_non,'S1','non',process_data.dc_pix_per_arcmin1)
    dset_s2_ao =data_new(ao_ram,'S2','ao',process_data.ram_pix_per_arcmin1)
    dset_s2_non=data_new(data_rs_non,'S2','non',process_data.ram_pix_per_arcmin1)
    dset_s3_ao =data_new(data_pt_all,'S3','ao',process_data.ram_pix_per_arcmin1)
    dset_s3_non=data_new(data_pt_non,'S3','non',process_data.ram_pix_per_arcmin1)

    ds_all=[dset_s1_ao, dset_s1_non, dset_s2_ao, dset_s2_non, dset_s3_ao, dset_s3_non]
    
    for ads in ds_all:
        data_trials=ads.data_trials
        data_trials['flip']=( (data_trials["resp"]=='f') & (data_trials["targ"]=='e') ) | ( (data_trials["resp"]=='e') & (data_trials["targ"]=='f')
                        ) | ( (data_trials["resp"]=='3') & (data_trials["targ"]=='E') ) | ( (data_trials["resp"]=='E') & (data_trials["targ"]=='3') )

    return ds_all

def load_complete():
	data_rs_non2 =util.dolist_raw([ 'Subjects/RS/nonao2/RS_ee_flanked_spacing_nonao_02072017-0%d.csv'%n for n in [1,2,3,4,5,6]]) 
	dset_rs_non2 =data_new(data_rs_non2,'S2','non',process_data.ram_pix_per_arcmin1)

	data_rs_non2_2 =util.dolist_raw([ 'Subjects/RS/nonao2017-2/RS_ee_flanked_spacing_nonao_02092017-0%d.csv'%n for n in [3,4,5,6,7]])
	dset_rs_non2_2=data_new(data_rs_non2_2,'S2','non',process_data.ram_pix_per_arcmin1)

	return dset_rs_non2, dset_rs_non2_2

def rs3():
    files1=rs_non_files
    files2=[ 'Subjects/RS/nonao2/RS_ee_flanked_spacing_nonao_02072017-0%d.csv'%n for n in [1,2,3,4,5,6]]
    files3=[ 'Subjects/RS/nonao2017-2/RS_ee_flanked_spacing_nonao_02092017-0%d.csv'%n for n in [3,4,5,6,7]]

    rs_non_files3=np.concatenate( (files1,files2,files3) )
    data_rs_non3 =util.dolist_raw(rs_non_files3)
    dset_rs_non3=data_new(data_rs_non3,'S3','non',process_data.ram_pix_per_arcmin1)

    return dset_rs_non3

