import pymc3 as pm
import pandas as pd
import numpy as np
import arvofit
import datetime # for timeit
import theano.tensor as tsr
import scipy

# Convenience functions to time execution (and display start time)
class timeit():
    from datetime import datetime
    def __enter__(self):
        self.tic = self.datetime.now()
        print('Start: {}').format(self.tic)
    def __exit__(self, *args, **kwargs):
        print('Runtime: {}'.format(self.datetime.now() - self.tic))

#import pymc3.math
def ptoz(p):
    z=pm.math.sqrt(2) * pm.math.erfinv( 2*p-1.0)
    return z

def ztop(z):
    p=0.5 * (1.0+pm.math.erf(z/pm.math.sqrt(2) ) )
    return p

def ztop_np(z):
    p=0.5 * (1.0+scipy.special.erf(z/np.sqrt(2) ) )
    return p

def ptoz_np(p):
    z=np.sqrt(2) * scipy.special.erfinv( 2*p-1.0)
    return z

priors={
    'asymptote_alpha':1.0,
    'asymptote_beta':1.0,
    'width_mu':4.0, #4.25,
    'width_sd':10.0, #1.5,
    'pse_mu':0.5,
    'pse_sd':0.5,
    'facslope_mu':0.5,
    'facslope_sigma':0.5,
    'faca_mu':1.0,
    'faca_sigma':1.0,
    'faca_alpha':1.0, # Favors things nearer to 1.0
    'faca_beta':1.0
}

def fit1(ads, priors=priors, whichsize=0):
    traces={}
    Xt={}
    Yt={}
    N={}

    print ads.who, ads.which, ads.ppm,
    for size in [ads.sizes[whichsize]]:
        Xt[size]=ads.data_sizes[size]['spacing'].values/ads.ppm
        Yt[size]=ads.data_sizes[size]['cor'].values
        N[size]=ads.data_sizes[size]['N'].values
        print size,

    pf={}
    Y_obs={}
    asympt={}
    facilitation_asymp={}

    basic_model=pm.Model()
    with basic_model:
        #    asymptote=pm.Beta('asymptote', alpha=priors['asymptote_alpha'], beta=priors['asymptote_beta'] )
        width=pm.Normal('width', mu=priors['width_mu'], sd=priors['width_sd'])
        pse=pm.Normal('pse', mu=priors['pse_mu'], sd=priors['pse_sd'])

        #sigg=pm.HalfNormal('sigg', sd=100.0)

        #facilitation_asymp=pm.Beta('facilitation_asymp', alpha=priors['faca_alpha'], beta=priors['faca_beta'])
        facilitation_slope=pm.Normal('facilitation_slope', mu=priors['facslope_mu'], sd=priors['facslope_sigma'])

        for size in [ads.sizes[whichsize]]:
            #facilitation_asymp[size]=pm.Beta('fa'+str(size), alpha=priors['faca_alpha'], beta=priors['faca_beta'])
            facilitation_asymp[size]=pm.Beta('fa'+str(size), alpha=priors['faca_alpha'], beta=priors['faca_beta'])
            asympt[size]=pm.Beta('asymptote'+str(size), alpha=priors['asymptote_alpha'], beta=priors['asymptote_beta'] )
            pf=arvofit.logistFN(Xt[size],asympt[size],width,pse,facilitation_asymp[size], facilitation_slope)
            p2=ztop( pf + ptoz(asympt[size] ) )
            #p2= ztop(pf) + asympt[size] 
            Y_obs[size] = pm.Binomial( 'Y_obs'+str(size), p=p2, n=N[size], observed=Yt[size])

            #pf=arvofit.logistF(Xt[size],asympt[size],width,pse,facilitation_asymp[size], facilitation_slope)
            #p2=(ptoz(pf-0.05))
            #Y_obs[size] = pm.Normal( 'Y_obs'+str(size), mu=p2, sd=sigg, observed=ptoz( Yt[size]/N[size]-0.05) )

        map_estimate = pm.find_MAP(model=basic_model)

        # Inference button (TM)!
        with timeit():
            step = pm.NUTS(model=basic_model)
        with timeit():
            traces[size] = pm.sample(2100, step=step, start=map_estimate)[100:]  

    ads.traces=traces
    ads.priors=priors
    ads.map_estimate=map_estimate
    ads.fit_time=format(datetime.datetime.now())

def cumul(X,guess,asymptote,width,pse,erf=scipy.special.erf, sqrt=np.sqrt):
    pf=guess+(asymptote-guess)*0.5*(1.0+erf( ( X-pse)/(width*sqrt(2)) )  )
    return pf

def cumul_fac_t(X,guess,asymptote,width,pse, facilitation_asymp, facilitation_slope):
    facil=facilitation_asymp-X*facilitation_slope
    crowded=cumul(X,guess,asymptote,width,pse,erf=tsr.erf, sqrt=tsr.sqrt)
    combined=tsr.maximum( facil,crowded )
    return combined

def cumul_fac_np(X,guess,asymptote,width,pse, facilitation_asymp, facilitation_slope):
    facil=facilitation_asymp-X*facilitation_slope
    crowded=cumul(X,guess,asymptote,width,pse) #,erf=scipy.special.erf, sqrt=np.sqrt):
    combined=np.max( (facil, crowded), 0)
    return combined

def priors():
  # Priors appropriate for proportion correct vs. spacing (arcmin)
  priors={
    'asymptote': {'name':'asymptote', 'alpha':1.0, 'beta':1.},
    'width': {'name':'width', 'sd':5.0}, 
    'pse': {'name':'pse', 'mu':0.1, 'sd':5.0},
    'facilitation_asymp': {'name':'facilitation_asymp', 'alpha':5.0, 'beta':1.0}, 
    'facilitation_slope': {'name':'facilitation_slope', 'alpha':1.0, 'beta':1.0},

    # Variations: (uyed for aggr)
    'facilitation_slope_normal': {'name':'facilitation_slope', 'mu':0.4, 'sd':0.05},
    'width_normal': {'name':'width', 'mu':0.25, 'sd':0.05}, 
  }
  return priors

def fit_cumul(ads,size, priors=priors(), boots=1000):
  dat=ads.data_sizes[size]
  Xt=dat['spacing'].values/ads.ppm
  Yt=dat['cor'].values
  N=dat['N'].values

  Xt[ dat['spacing']>60] = 3.1

  guess=0.25

  basic_model=pm.Model()
  with basic_model:
    asymptote=pm.Beta(**priors['asymptote'])
    width=pm.HalfNormal(**priors['width'])
    pse=pm.Normal(**priors['pse'] )
    facilitation_asymp=pm.Beta(**priors['facilitation_asymp'])
    facilitation_slope=pm.Beta(**priors['facilitation_slope'])

    pf=cumul_fac_t(Xt,guess,asymptote,width,pse,facilitation_asymp,facilitation_slope)
    
    Y_obs = pm.Binomial( 'Y_obs', p=pf, n=N, observed=Yt)
   
    map_estimate = pm.find_MAP(model=basic_model)

    ads.map_estimate[size]=arvofit.fix_params(map_estimate)
    ads.priors=priors

    if boots>0:
        with timeit():
            step = pm.NUTS(model=basic_model)
        with timeit():
            traces = pm.sample(boots, step=step, start=map_estimate)
    
        ads.traces[size]=traces
    else:
        traces=[]

    return map_estimate,basic_model,(Xt,Yt,N),traces

def fit_cumul_aggr(ads,size, priors=priors(), boots=1000):
  # Fit to a cumul gaussian that is shifted down to the range -1:0
  dat=ads.data_sizes[size]
  Xt=dat['spacing'].values/ads.ppm
  Yt=dat['cor'].values
  N=dat['N'].values

  Xt[ dat['spacing']>60] = 3.1
  guess=0.25

  basic_model=pm.Model()
  with basic_model:
    asymptote=pm.Beta(**priors['asymptote'])
    width=pm.Normal(**priors['width_normal'])
    pse=pm.Normal(**priors['pse'] )
    facilitation_asymp=pm.Beta(**priors['facilitation_asymp'])
    #facilitation_slope=pm.Beta(**priors['facilitation_slope'])
    facilitation_slope=pm.Normal(**priors['facilitation_slope_normal'])

    # No guess rate for the arbitrary reduction fit
    pf=cumul_fac_t(Xt,0.0,1.0,width,pse,facilitation_asymp,facilitation_slope)
    
    #est=( ptoz(pf)*0.5+1+(1.0-asymptote) ) 
    est=ztop( 2.0*(pf-1.0) + ptoz(asymptote) )

    #print est
    Y_obs = pm.Binomial( 'Y_obs', p=est, n=N, observed=Yt)
   
    map_estimate = pm.find_MAP(model=basic_model)

    ads.map_estimate[size]=arvofit.fix_params(map_estimate)
    ads.priors=priors

    if boots>0:
        with timeit():
            step = pm.NUTS(model=basic_model)
        with timeit():
            traces = pm.sample(boots, step=step, start=map_estimate)
    
        ads.traces[size]=traces
    else:
        traces=[]

    return map_estimate,basic_model,(Xt,Yt,N),traces

def valley1(parms):
    xr=np.linspace(0,3)
    pf=cumul_fac_np(xr,0.25, parms['asymptote'],
        parms['width'], parms['pse'],
        parms['facilitation_asymp'],
        parms['facilitation_slope'])

    minval = pf.min()
    minloc = xr[ pf==minval][0]
    return minval, minloc

def valleys(ads, nsize=0):
    xr=np.linspace(0,3)
    vals=[]
    locs=[]
    atrace=ads.traces[ads.sizes[nsize] ]
    for trace1 in atrace:
        minval,minloc=valley1(arvofit.fix_params(trace1) )
        vals = np.concatenate( (vals,[minval]) )
        locs = np.concatenate( (locs,[minloc]) )
        
    return vals,locs
