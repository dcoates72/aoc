import numpy as np

# xyscan Version 3.3.3
# Date: Wed Mar 22 22:23:01 2017
# Scanned by: dcoates
# Source: drasdo2007.png
# Comment: 
# Format: x y -dx +dx -dy +dy
pts=np.array([ [13.9583,	0.093589	],
[11.9583,	0.0814417	],
[8.95833,	0.0640491	],
[5.95833,	0.0458282	],
[4,	0.0339571	],
[2.95833,	0.0278834	],
[2,	0.0218098	],
[1,	0.0157362	],
[0.5,	0.0138037],
[4.44089e-16,	0.00883436] 
] )
