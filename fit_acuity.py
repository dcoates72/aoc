#from scipy.special import erf
from theano.tensor import erf

def cumulative_gaussian(X,sigma,mu,guess=0.25):
    fun=0.5*(1.0+erf( (X-mu)/sigma*np.sqrt(2)))
    pf=guess+(1.0-guess)*fun
    return pf

def pdf_gaussian(X,sigma,mu,guess=0.25):
    pdf=1.0/np.sqrt(2*sigma**2*np.pi)*np.exp(
	-(X-mu)**2/(2*sigma**2) )
    return pdf

#from theano import config
#config.device = 'gpu0'
#config.floatX = 'float32'
#config.optimizer = 'fast_compile'

from pymc3 import *
def fitgauss(Xt,Yt,guess=0.25, mu_mu=-0.5, mu_sd=10, sigma_mu=0.15, sigma_sd=0.20, traceN=0):
  basic_model=Model(verbose=2)
  with basic_model:
    mu=Normal('mu', mu=mu_mu, sd=mu_sd )
    sigma=Normal('sigma', mu=sigma_mu, sd=sigma_sd)

    pf=cumulative_gaussian(Xt,sigma,mu,guess) 
    Y_obs = Binomial( 'Y_obs', p=pf, n=10, observed=Yt*10)
   
    map_estimate = find_MAP(model=basic_model)
    print map_estimate

    if traceN>0:
        trace = sample(traceN, start=map_estimate)
    else:
        trace=[]

    return map_estimate,basic_model,trace

#map_estimate,basic_model=fitwork( res[2], res[3], pse1_sd=2.0 )
#print map_estimate
