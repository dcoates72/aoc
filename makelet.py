import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

RANDOM_LETTER=chr(200)
RANDOM_NUMBER=chr(201)
RANDOM_T=chr(202)
RANDOM_BAR=chr(203)
RANDOM_E=chr(204)

# Codes for 'LCD' segments:
# 000
# 3 5
# 111
# 4 6
# 222
#
# 7: Vertical stem in middle taking whole col.
charbits={
    'A': [ 1,1,0,1,1,1,1,0 ], #A, 0  *
    'B': [ 0,0,0,0,0,0,0,0 ], #B, 1
    'C': [ 1,0,1,1,1,0,0,0 ], #C, 2  *
    'D': [ 0,0,0,0,0,0,0,0 ], #D, 3
    'E': [ 1,1,1,1,1,0,0,0 ], #E, 4  *
    'F': [ 1,1,0,1,1,0,0,0 ], #F, 5  *
    'G': [ 0,0,0,0,0,0,0,0 ], #G, 6
    'H': [ 0,1,0,1,1,1,1,0 ], #H, 7  *
    'I': [ 1,0,1,0,0,0,0,1 ], #I,
    'J': [ 0,0,1,0,1,1,1,0 ], #J, 9  *
    'K': [ 0,0,0,0,0,0,0,0 ], #K, 10
    'L': [ 0,0,1,1,1,0,0,0 ], #L, 11 *
    'M': [1,0,0,1,1,1,1,1],#E rot: line on top
    'N': [ 0,0,0,0,0,0,0,0 ], #N, 13
    'O': [ 1,0,1,1,1,1,1,0 ], #O, 14 *
    'P': [ 1,1,0,1,1,1,0,0 ], #P, 15 *
    'Q': [ 0,0,0,0,0,0,0,0 ], #Q, 16
    'R': [ 0,0,0,0,0,0,0,0 ], #R, 17
    'S': [ 1,1,1,1,0,0,1,0 ], #S, 18 *
    'T': [ 1,0,0,0,0,0,0,1 ], #T, *
    'U': [ 0,0,1,1,1,1,1,0 ], #U, 20 *
    'V': [ 0,0,0,0,0,0,0,0 ], #V, 21
    'W': [0,0,1,1,1,1,1,1],#E_rot: line on btm
    'X': [ 0,0,0,0,0,0,0,0 ], #X, 23
    'Y': [ 0,0,0,0,0,0,0,0 ], #Y, 24
    'Z': [ 0,0,0,0,0,0,0,0 ], #Z, 25

    '0': [ 1,0,1,1,1,1,1,0 ], #0
    '1': [ 0,0,0,0,0,0,0,1 ], #1
    '2': [ 1,1,1,0,1,1,0,0 ], #2
    '3': [ 1,1,1,0,0,1,1,0 ], #3
    '4': [ 0,1,0,1,0,1,1,0 ], #4
    '5': [ 1,1,1,1,0,0,1,0 ], #5
    '6': [ 1,1,1,1,1,0,1,0 ], #6
    '7': [ 1,0,0,0,0,1,1,0 ], #7
    '8': [ 1,1,1,1,1,1,1,0 ], #8
    '9': [ 1,1,1,1,0,1,1,0 ], #9

    'u': [ 1,0,0,0,0,0,0,1 ], #T ('upright')
    'd': [ 0,0,1,0,0,0,0,1 ], #T rot 180 deg CCW ('down')
    'l': [ 0,1,0,1,1,0,0,0 ], #T rot  90 deg CCW ('left')
    'r': [ 0,1,0,0,0,1,1,0 ], #T rot 270 deg CCW ('right')

    'x': [ 1,1,1,1,1,1,1,1 ], #all ones

    'e': [1,0,0,1,1,1,1,1],#E rot: line on top
    'f': [0,0,1,1,1,1,1,1],#E_rot: line on btm

    'h': [1,1,1,0,0,0,0,0],# horizontal 'grating'
    'v': [0,0,0,1,1,1,1,1],# vertical 'grating'

    ' ': [0,0,0,0,0,0,0,0],
}

set_numbers=np.array(['0','1','2','3','4','5','6','7','8','9'])
set_letters=np.array(['A','C','E','F','H','I', 'J','L','O','P','S','T','U'])
set_T=np.array(['u','d','l','r'])
set_bar=np.array(['v','h'])
set_E=np.array(['E','3','e','f'])

def random_char(which):
    if which==RANDOM_LETTER:
        whichset=set_letters
    elif which==RANDOM_NUMBER:
        whichset=set_numbers
    elif which==RANDOM_T:
        whichset=set_T
    elif which==RANDOM_BAR:
        whichset=set_bar
    elif which==RANDOM_E:
        whichset=set_E

    whichidx=np.random.randint(len(whichset))
    which=whichset[whichidx]
    return which

def make_let(whichlet, size=10, buf=None, size_buf=100, loc_x=0, loc_y=0, size_bar=0, spacing=10, polarity=0):
    try:
        size_buf=np.shape(buf)[0]
    except IndexError:
        buf = np.zeros( (size_buf,size_buf) )

    # Identify coordinates of interest (coordinates inside buffer)
    midl = size_buf/2-size/2
    tl = midl-size*2
    br = midl+size*2 # inner edge
    br2 = br+size #outer edge

    if ord(whichlet) >= ord(RANDOM_LETTER): # anything large is random
        whichlet=random_char(whichlet)
        stim_def=charbits[whichlet]
    else:
        stim_def = charbits[whichlet]

    # Set bits based on features in character
    if stim_def[0]: #L
        buf[loc_y+tl:loc_y+tl+size,loc_x+tl:loc_x+br2]=1
    if stim_def[1]: #R
        buf[loc_y+midl:loc_y+midl+size,loc_x+tl:loc_x+br2]=1
    if stim_def[2]:
        buf[loc_y+br:loc_y+br2,loc_x+tl:loc_x+br2]=1
    if stim_def[3]:
        buf[loc_y+tl:loc_y+midl,loc_x+tl:loc_x+tl+size]=1
    if stim_def[4]:
        buf[loc_y+midl:loc_y+br2,loc_x+tl:loc_x+tl+size]=1
    if stim_def[5]:
        buf[loc_y+tl:loc_y+midl,loc_x+br:loc_x+br2]=1
    if stim_def[6]:
        buf[loc_y+midl:loc_y+br2,loc_x+br:loc_x+br2]=1
    if stim_def[7]:
        buf[loc_y+tl:loc_y+br2,loc_x+midl:loc_x+midl+size]=1

    # bar flankers
    buf[loc_y+br2+spacing:loc_y+br2+spacing+size_bar,loc_x+tl:loc_x+br2]=1 # bottom
    buf[loc_y+tl-spacing-size_bar:loc_y+tl-spacing,loc_x+tl:loc_x+br2]=1 # top
    buf[loc_y+tl:loc_y+br2,loc_x+tl-spacing-size_bar:loc_x+tl-spacing]=1 # left
    buf[loc_y+tl:loc_y+br2,loc_x+br2+spacing:loc_x+br2+spacing+size_bar]=1 # right

    if polarity!=0:
        buf[buf==1]=-1
        buf+=1

    return buf,whichlet

def bufshow(buf):
    plt.imshow( buf, interpolation='Nearest', cmap=cm.bone)
    #plt.show()

def plotlet(whichlet, size=10, size_buf=100, loc_x=0, loc_y=0, size_bar=0, spacing=10):
    buf,whichlet = make_let(whichlet, size=size, size_buf=size_buf, size_bar=size_bar, spacing=spacing)
    bufshow(buf)

def quad(target, flankers, cc_spacing, size=10, size_buf=100,polarity=0, show=True):
    buf_t=np.zeros( (size_buf, size_buf))
    buf_f=np.zeros( (size_buf, size_buf))
    _,let_t=make_let(target, buf=buf_t, size=size, loc_x=0, loc_y=0, size_bar=0)

    _,let_f1=make_let(flankers, buf=buf_f, size=size, loc_x=-cc_spacing, size_bar=0)
    _,let_f2=make_let(flankers, buf=buf_f, size=size, loc_x=+cc_spacing, size_bar=0)
    _,let_f3=make_let(flankers, buf=buf_f, size=size, loc_y=-cc_spacing, size_bar=0)
    _,let_f4=make_let(flankers, buf=buf_f, size=size, loc_y=+cc_spacing, size_bar=0)

    if polarity!=0:
        buf_t[buf_t==1]=-1
        buf_t+=1
        buf_f[buf_f==1]=-1
        buf_f+=1

    buf=buf_t+buf_f
    if show:
        bufshow(buf)
    return buf,[let_t,let_f1,let_f2,let_f3,let_f4],buf_t,buf_f

def square( center, rad, style):
    plt.plot( [center-rad/2., center-rad/2.0, center+rad/2.0, center+rad/2.0, center-rad/2.0],
        [center-rad/2.0,center+rad/2.0,center+rad/2.0,center-rad/2.0,center-rad/2.0], style )

