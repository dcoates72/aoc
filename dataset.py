import arvofit
import numpy as np

def doparams(ds):
    ds.params=arvofit.auto_trace_grid(ds.fits, ds.data, '%s %s'%(ds.subject,ds.condition))

def do_cs(ds):
    ds.cs=arvofit.cond_summary_plot( ds.fits, ds.data, ds.ppm,'%s %s'%(ds.subject,ds.condition))

def do_unflanked(ds,traceN=1000):
    xvals = np.log10( ds.sizes/ds.ppm )
    yvals = ds.params['asymptote']
    ds.unflanked=arvofit.fit_acuity1(xvals,yvals, doplot=True, dotrace=True, dotraceplot=True, traceN=traceN )

class dataset(object):
    #sizes = None
    #data = None
    #fits = None
    #ppm = None
    #name = None
    dotraceplot=False
    traceN=1000
    subfix=""

    # Previous defaults:
    #map_estimate,model=fitwork( x_tofit, y_tofit, N=N, pse_mu=0.25, pse1_sd=0.5,
                                #width_mu=4.6, width_sd=0.2,
                                #fac_sd=1.0, facilitation_slope_sd=1.0)
    # These over-rode these:
#def fitwork(Xt,Yt,guess=0.25,N=10,asymptote_alpha=1.0,asymptote_beta=1.0,width_sd=5.0,width_mu=0.5,pse1_sd=100.0,
            #pse_mu=1.0, facilitation_slope_mu=1.0, facilitation_slope_sd=10.0, fac_sd=0.4, facilitation_asymptote_alpha=1.0,facilitation_asymptote_beta=1.0):

    priors={ 'asymptote_alpha':1.0, 'asymptote_beta':1.0, #TODO: fix PF to use asymptote correctly

                'width_sd':5.0, # Normal for Gaussian width/slope
                'width_mu':0.5,
                
                'pse1_mu':1.0, # Normal for PSE
                'pse1_sd':100.0,

                'facilitation_asymptote_alpha':1.0, # Beta for facilitation asymptote
                'facilitation_asymptote_beta':1.0, 

                'facilitation_slope_sd':10, # HalfNormal for slope of facilitation
    }

    def __init__(self, subject, condition, ppm, sizes, data):
        self.subject=subject
        self.condition=condition
        self.ppm=ppm
        self.sizes=sizes
        self.data=data

    def dofit(self):
        self.fits=arvofit.autosizes(self.data, self.ppm, self.dotraceplot, self.traceN, self.subfix)

class dataset(object):
    #sizes = None
    #data = None
    #fits = None
    #ppm = None
    #name = None
    dotraceplot=False
    traceN=1000
    subfix=""

    def __init__(self, subject, condition, ppm, sizes, data):
        self.subject=subject
        self.condition=condition
        self.ppm=ppm
        self.sizes=sizes
        self.data=data

    def dofit(self, traceN=1000):
        self.traceN=traceN
        self.fits=arvofit.autosizes(self.data, self.ppm, self.dotraceplot, self.traceN, self.subfix)

