import pymc3 as pm
import pandas as pd
import numpy as np
import arvofit
import datetime # for timeit
import theano.tensor as tsr
import scipy
import scipy.stats as st # for kde

# Convenience functions to time execution (and display start time)
class timeit():
    from datetime import datetime
    def __enter__(self):
        self.tic = self.datetime.now()
        print('Start: {}').format(self.tic)
    def __exit__(self, *args, **kwargs):
        print('Runtime: {} (End:{})'.format(self.datetime.now() - self.tic, self.datetime.now()))

#import pymc3.math
def ptoz(p):
    z=pm.math.sqrt(2) * pm.math.erfinv( 2*p-1.0)
    return z

def ztop(z):
    p=0.5 * (1.0+pm.math.erf(z/pm.math.sqrt(2) ) )
    return p

def ztop_np(z):
    p=0.5 * (1.0+scipy.special.erf(z/np.sqrt(2) ) )
    return p

def ptoz_np(p):
    #try:
        #p -= 0.05
        z=np.sqrt(2) * scipy.special.erfinv( 2*p-1.0)
    #except TypeError:
        #print p
        return z

def logistic(X,guess,asymptote,width,pse,erf=scipy.special.erf, sqrt=np.sqrt):
    pf=guess+(asymptote-guess)*(1./(1+np.exp(-width*(X-pse) ) ) )
    return pf

def cumul(X,guess,asymptote,width,pse,erf=scipy.special.erf, sqrt=np.sqrt):
    pf=guess+(asymptote-guess)*0.5*(1.0+erf( ( X-pse)/(width*sqrt(2)) )  )
    return pf

# For this variant, keep facilitation outside of the z transform.
# Basic cumulative Gaussian goes from 0 to 1, and is shifted down to -1 to 0 then scaled.
def cumul_fac2_t(X,guess,asymptote,width,pse, facilitation_asymp, facilitation_slope, xform_z=True):
    facil=facilitation_asymp-X*facilitation_slope
    crowded=cumul(X,0.0,1.0,width,pse,erf=tsr.erf, sqrt=tsr.sqrt)
    #crowded_transform=( 2.0*(crowded-1.0) + (asymptote) )
    if xform_z:
        crowded_transform=( 2.0*(crowded-1.0) + (asymptote) )
        combined=ztop( tsr.maximum( facil,crowded_transform ) )
    else:
        crowded=( crowded*ztop(asymptote) )
        combined=tsr.maximum( ztop(facil),crowded)

    return combined

def cumul_fac2_np(X,guess,asymptote,width,pse, facilitation_asymp, facilitation_slope, xform_z=True):
    facil=facilitation_asymp-X*facilitation_slope
    crowded=cumul(X,0.0,1.0,width,pse)
    if xform_z:
        crowded_transform=( 2.0*(crowded-1.0) + (asymptote) )
        combined=ztop_np( np.max( (facil,crowded_transform), 0 ) )
    else:
        crowded=(crowded*ztop_np(asymptote) )
        combined=np.max( (ztop_np(facil),crowded), 0 )
    return combined

def priors():
  # Priors appropriate for proportion correct vs. spacing (arcmin)
  priors_logistic={
    'asymptote': {'name':'asymptote', 'alpha':1.0, 'beta':1.0},
    'width': {'name':'width', 'sd':100.0}, 
    'pse': {'name':'pse', 'sd':100.0},
    'facilitation_asymp': {'name':'facilitation_asymp', 'alpha':2.0, 'beta':1.0}, 
    'facilitation_slope': {'name':'facilitation_slope', 'alpha':1.0, 'beta':1.0},
  }

  priors_cumulative={
    'asymptote': {'name':'asymptote', 'alpha':1.0, 'beta':1.0, 'type':pm.Beta},
    #'asymptote': {'name':'Zasymptote', 'mu':1.0, 'sd':100.0,'type':pm.Normal}, #normal
    # 'width': {'name':'width', 'alpha':1.0, 'beta':1.0, 'type':pm.Beta},  #3
    #'width': {'name':'width', 'alpha':1.0, 'beta':1.0, 'type':pm.Beta},  #3
    #'width': {'name':'width', 'sd':5.0, 'type':pm.HalfNormal},  #3
    #'width': {'name':'width', 'alpha':1.0, 'beta':1.0, 'type':pm.Beta},  #3

    # TODO: Notsure which of these is correct!!

    #'width': {'name':'width', 'mu':0.05, 'sd':0.01, 'type':pm.Normal},  #3 # good prior for middl/steepr
    'width': {'name':'width', 'mu':0.15, 'sd':0.2, 'type':pm.Normal},  #3

    #'pse': {'name':'pse', 'mu':0.5, 'sd':100.0, 'type':pm.Normal},
    'pse': {'name':'pse', 'alpha':1.0, 'beta':1.0, 'type':pm.Beta},
    'facilitation_asymp': {'name':'facilitation_asymp', 'alpha':1.0, 'beta':1.0, 'type':pm.Beta}, 
    #'facilitation_asympZ': {'name':'Zfacilitation_asymp', 'sd':10.0, 'type':pm.HalfNormal},  
    'facilitation_asympZ': {'name':'facilitation_asymp', 'alpha':1.0, 'beta':1.0, 'type':pm.Beta}, 
    #'facilitation_asympZ': {'name':'Zfacilitation_asymp', 'sd':10.0, 'type':pm.HalfNormal},  
    'facilitation_slope': {'name':'facilitation_slope', 'alpha':1.0, 'beta':1.0, 'type':pm.Beta},

    # Variations: (uyed for aggr)
    'facilitation_slope_normal': {'name':'facilitation_slope', 'sd':5.0, 'type':pm.HalfNormal},
    'width_normal': {'name':'width', 'mu':0.4, 'sd':0.1}, 
    'facilitation_asymp_normal': {'name':'facilitation_asymp', 'mu':0.84, 'sd':0.2}, 
  }

  return priors_cumulative

def valley1(parms):
    xr=np.linspace(0,3)
    pf=cumul_fac2_np(xr,0.0, parms['asymptote'],
        parms['width'], parms['pse'],
        parms['facilitation_asymp'],
        parms['facilitation_slope'])

    minval = pf.min()
    minloc = xr[ pf==minval][0]
    return minval, minloc

xr=np.linspace(0,3,500)
def cs(parms, xform=False, raw_fit_probit_cs=False,xr=xr):
    if xform==False:
        pf=cumul_fac_np(xr, 0.25,  parms['asymptote'],
            parms['width'], parms['pse'],
            parms['facilitation_asymp'],
            parms['facilitation_slope'])

    if raw_fit_probit_cs:
        maxval = ptoz_np(pf.max())-0.05
        maxloc = xr[ ptoz_np(pf)>maxval][0]
    else:
        maxval = pf.max()*0.95
        maxloc = xr[ pf>(maxval)][0]
    return maxloc,maxval

def cs2018(xr, pf, thresh=0.025):
    # Need to skip facilitation region. To do so, find an index past 0.5
    if thresh>0.05:
        maxval = ptoz_np(pf[-1])-thresh 
    else:
        maxval = pf[-1]-thresh 

    #maxloc = xr[::-1][ pf[startat:]>(maxval)][0]
    try:
        if thresh>0.05:
            maxloc = xr[::-1][ptoz_np( pf[::-1])<maxval ] [0]
        else:
            maxloc = xr[::-1][ pf[::-1]<maxval ] [0]
    except:
        print ("bad, vals=%0.2f"%pf[-1])
        maxloc=-1

    return maxloc

def peak2018(arr,kde_points=1000):
    range_kde=np.linspace(arr.min(), arr.max(), kde_points)
    kern=st.gaussian_kde( arr )
    fits=kern(range_kde)
    maxloc=range_kde[fits.max()==fits]
    return maxloc

def valleys(ads, nsize=0):
    xr=np.linspace(0,3)
    vals=[]
    locs=[]
    atrace=ads.traces[ads.sizes[nsize] ]
    for trace1 in atrace:
        minval,minloc=valley1(arvofit.fix_params(trace1) )
        vals = np.concatenate( (vals,[minval]) )
        locs = np.concatenate( (locs,[minloc]) )
        
    return vals,locs

def num_prior( priors, which, number, maketype=False ):
    priors_copy=priors[which].copy() # copy the dictionary
    priors_copy['name'] += str(number)
    #priors_copy['name'] += priors_copy['name'] + str(number)

    try:
        del priors_copy['type']
    except KeyError:
        pass # ok, just ignore if key not there

    if maketype:
        return priors[which]['type'](**priors_copy)
    else:
        return priors_copy

def addnum(s,n):
    return s+str(n)

map_cond = [0,0, 1,1,
        2,2,2,2,2, 3,3,3,
        4,4,4,4, 5,5,5,5 ]
map_sub = [0,0, 0,0,
        1,1,1,1,1, 1,1,1,
        2,2,2,2, 2,2,2,2 ]
map_sizes = [0,1, 2,3,
        4,5,6,7,8, 9,10,11,
        12,13,14,15, 16,17,18,19 ]
map_sub_first = [0,2,4,9,12,16,20]
def map_none(simuls):
    return np.arange(simuls) 

def get_yokemaps(yokes,simuls):
    # These index maps convert from condition to paramset
    # For our original AOC data
    map_all = [0]*simuls

    map_sub_p = np.random.permutation( map_sub )
    map_cond_p = np.random.permutation( map_cond )

    maps = {'none': map_none(simuls),
            'cond': map_cond,
            'subs' : map_sub,
            'all' : map_all,
            'sizes' : map_sizes,
            'subs_shuffle': map_sub_p,
            'cond_shuffle': map_cond_p }

    # [width,pse,fslopes]
    yokemaps = [maps[ayoke] for ayoke in yokes]
    return yokemaps
        

def fit_cumul_all(ds_set, priors=priors(), boots=1000, njobs=1, yokes="", scaled_fa=False, xform_z=True):

    Xt={}
    Yt={}
    N={}
    which_idx=0
    idxs={}
    for nds,ads in enumerate( ds_set ):
        idxs[nds]=which_idx # remember first size of each condition
        for nsize,size in enumerate(ads.sizes):
            #print ads,nsize,size
            dat=ads.data_sizes[size]
            Xt[which_idx]=dat['spacing'].values/ads.ppm
            Yt[which_idx]=dat['cor'].values
            N[which_idx]=dat['N'].values
            which_idx += 1
            #Xt[ dat['spacing']>60] = 3.1

    guess=0.0 # BIG PROBLEM

    simuls=len(N)
    yokemaps=get_yokemaps(yokes,simuls)

    if True:
        if True:
            basic_model=pm.Model()
            with basic_model:

                asymptotes=[num_prior(priors,'asymptote',n,maketype=True) for n in np.unique(map_none(simuls)) ] 
                widths=[num_prior(priors, 'width',n,maketype=True) for n in np.unique(yokemaps[0]) ]
                pses=[num_prior(priors, 'pse',n,maketype=True) for n in np.unique(yokemaps[1]) ]

                if scaled_fa==False:
                    facilitation_asymps=[num_prior(priors,'facilitation_asympZ',n,maketype=True) for n in np.unique(map_none(simuls)) ]
                else:
                    # Priors for the linear relationship between asymptote and facilitation asymptote
                    #fa_slope=pm.Normal("fa_slope",mu=0.8,sd=5)
                    fa_slope=pm.Beta("fa_slope",alpha=1.0,beta=1.0)
                    fa_intercept=pm.Normal("fa_intercept",mu=0,sd=1.0)
                    facilitation_asymps=asymptotes*fa_slope+fa_intercept

                #facilitation_slopes=[pm.Beta(**num_prior(priors,'facilitation_slope',n)) for n in np.unique(yokemaps[2]) ]
                # Following demonstrates slick new way to instantiate based on type in the prior itself. Cool!
                facilitation_slopes=[num_prior(priors,'facilitation_slope',n,maketype=True) for n in np.unique(yokemaps[2]) ]

                # See also analysis2018.pfstats
                #wid=widths[0]+(1.0-asymptotes[n])*widths[1] 
                wid=widths[yokemaps[0][n]]
                pf=[cumul_fac2_t(Xt[n],guess,asymptotes[n]*10.-5.,wid*5,pses[yokemaps[1][n]]*2.0-1.0,
                    facilitation_asymps[n]*10.0-5.0, facilitation_slopes[yokemaps[2][n]], xform_z=xform_z ) for n in np.unique(map_none(simuls)) ]

                Y_obs =[ pm.Binomial( addnum('Y_obs',n), p=0.25+(pf[n]*0.75), n=N[n], observed=Yt[n]) for n in np.unique(map_none(simuls)) ]

                with timeit():
                    map_estimate = pm.find_MAP(model=basic_model)

                try:
                    ads.map_estimate[size]=arvofit.fix_params(map_estimate)
                except AttributeError:
                    ads.map_estimate={}
                    ads.map_estimate[size]=arvofit.fix_params(map_estimate)
    
                ads.priors=priors

                if boots>0:
                    with timeit():
                        step = pm.NUTS(model=basic_model)
                    with timeit():
                        traces = pm.sample(boots, step=step, start=map_estimate, njobs=njobs)

                    ads.traces=traces
                else:
                    traces=[]

            return map_estimate,basic_model,(Xt,Yt,N),yokes,priors,traces

conds=20
# Return information about the condition (of 19)
def cond2xxx(ds_all,n):
    who=map_sub[n]
    whowhat=map_cond[n]
    nsiz=n-map_sub_first[whowhat]
    ads=ds_all[whowhat]
    siz= int(ads.sizes[nsiz])
    return who,whowhat,nsiz,siz,ads
