import psychopy.visual as visual
import numpy as np
#import params2 as params


################################################
# Helpers
################################################
# Rings:
def ring_position(center, radius, N, which_angle, jitter_amt=0.0, phase=0):
	angle=2*np.pi/N*which_angle+phase
	x=center[0]+np.cos(angle)*radius+jitter_amt*(np.random.rand()*2.0-1.0)
	y=center[1]+np.sin(angle)*radius+jitter_amt*(np.random.rand()*2.0-1.0)
	return (x,y)

# Polar Positions starting from (0th=0deg, at 3 o'clock. Rightmost. counter-clockwise)
def make_ring(center, radius, N, phase=0, jitter_amt=0):
	return [ring_position( center, radius, N, which_angle, phase=phase, jitter_amt=jitter_amt) for which_angle in range(N)]

################################################
# Base class and single-object classes
################################################
class Stim(object):
	# Base class stim object. Doesn't do much, except calling underlying
	# "_stim" object (psychopy object)'s draw method, which is often sufficient.
	def __init__(self, win=None):
		self.params=params
		self.win=win
		self._stim=None
	# In drawing loop. Should be fast.
	def draw(self):
		if not(self._stim==None):
			self._stim.draw()
	def __repr__(self):
		return "Stim({0.params!r})".format(self)
	# For trials, update things
	def update(self):
		pass

class Letter(Stim):
	def __init__(self, win, name, font, pos, size, text, ori, color):
		# Prepend with "_" internal (non-serialized) things
		if not(win==None):
			self._stim=visual.TextStim( win, pos=pos, alignHoriz='center', alignVert='center', units='pix',
				height=size, color=color, ori=ori, text=text, font=font)
			self.win=win
		else:
			self.win=None
			self._stim=None

		self.name=name
		self.font=font
		self.pos=pos
		self.size=size
		self.text=text
		self.ori=ori
		self.color=color
		return
	def __repr__(self):
		return 'Letter({0.name!r}, {0.font!r}, {0.pos!r}, {0.size!r}, {0.text!r}, {0.ori!r}, {0.color!r})'.format(self)
	# For each trial, set up stuff
	def update(self):
		return

class Ring(object):
	def __init__(self, pos, ecc, N ):
		self.pos=pos
		self.ecc=ecc
		self.N=N

class StimSet(object):
	def __init__(self,stims=[]):
		self._stims=stims
	def draw(self):
		[astim.draw() for astim in self._stims]
	def update(self, val, trialNum):
		[astim.update(val, trialNum) for astim in self._stims]

class LetterRing(StimSet):
	def __init__(self,name,win=None, params=None, radius=None, center=(0,0), text="X", spokes=None):
		self.params=params
		if radius==None:
			radius=self.params.radius
		if spokes==None:
			spokes=self.params.num_spokes
		self._stims=[Letter(win,name="%s_%d"%(name,npos),font=self.params.font, pos=apos, size=self.params.size, text=text, ori=self.params.ori, 
			color=self.params.color_def) for (npos,apos) in enumerate(make_ring(center, radius,
				spokes, phase=self.params.phase, jitter_amt=self.params.jitter_amt) ) ] 
	def update(self,val,trialNum):
		for astim in self._stims:
			astim._stim.height=val
		# TODO: adjust nominal spacing of flankers.. should not be here I think though.
		#if self.params.variable=="size":
			#[aflanker.setHeight(val) for astim in self._stims]
			#[aflanker.setHeight(val) for astim in self._stims]

def oddball_select(num_spokes):
	quot=num_spokes/8.0 
	if num_spokes>=8:
		target_element = np.random.randint(0,8)*quot
		target_response=target_element/quot
	else:
		target_element = np.random.randint(0,num_spokes)
		target_response=target_element/quot
	return target_element,target_response

class OddballLetterRing(LetterRing):
	def __init__(self,name,win=None,params=None, radius=None,center=(0,0) ):
		self.params=params
		if radius==None:
			radius=self.params.radius
		LetterRing.__init__(self,name,win,params=params,radius=radius,center=center)
		if self.params.foveal_helper:
			self._helper=Letter(win,name="Helper",font=self.params.font, pos=center,
				size=self.params.size, text="X", ori=self.params.ori, color=self.params.color_def)
	def update(self,val,trialNum):
		# oddball paradigm
		# Random letters:
		if self.params.letter_set=="Sloan":
			#letters_possible=['C','D','H','K','N','O','R','S','V','Z']
			#letters_possible=['O','C','P','R','S','N','H','W','M','Z']
			letters_possible=['C','C','S','S','P','P','R','R','O','O']
		if self.params.letter_set=="custom":
			letters_possible=self.params.letter_set_custom #['C','S','O','P','R']
		else:
			letters_possible=[chr(c) for c in np.arange( ord('A'),ord('Z')+1 )]

		if (self.params.target_sequence=="balanced") and self.params.char_target_rand:
			target_let=letters_possible[ self.params.target_seq[trialNum]%10 ] # TODO: only works <10 set
			untarget_let=letters_possible[ self.params.target_seq[trialNum]/10 ]
		elif self.params.char_target_rand:
			target_let=letters_possible[np.random.randint( len(letters_possible) )]
			# Pick a different letter for untarget:
			untarget_let=target_let
			while( untarget_let==target_let):
				untarget_let=letters_possible[np.random.randint( len(letters_possible) )]
		elif (self.params.char_target=="flip"):
			target_let=self.params.char_untarget
			untarget_let=self.params.char_untarget
		elif (self.params.char_untarget=="flip"):
			target_let=self.params.char_target
			untarget_let=self.params.char_target
		else:
			target_let=self.params.char_target
			untarget_let=self.params.char_untarget
		self.target_let=target_let
		self.untarget_let=untarget_let

		(target_element,self.target_which)=oddball_select(self.params.num_spokes)
		for nangle,atarg in enumerate(self._stims):
			astim=atarg._stim
			astim.ori = self.params.ori #np.random.randint(0,360+1 ) #180
			if self.params.variable=="size":
				astim.height = val #np.random.randint(0,360+1 ) #180

			if nangle==target_element:
				astim.setText( target_let )
				if (self.params.char_target=="flip"):
					astim.ori += 180
			else:
				astim.setText( untarget_let )
				if (self.params.char_untarget=="flip"):
					astim.ori += 180

		if self.params.variable=="size":
			if self.params.foveal_helper:
				self._helper._stim.setHeight(val) 

		if self.params.foveal_helper==True:
			if self.params.helper_identity_flanker:
				self._helper._stim.setText('X')
			elif self.params.helper_identity_uninformative:
				uninf=target_let
				while (uninf==target_let) or (uninf==untarget_let):
					uninf=np.random.permutation(letters_possible)[0]
				self._helper._stim.setText(uninf)
			else: # normal case: cue target
				self._helper._stim.setText(target_let)
			if self.params.helper_polarity_reverse:
				self._helper._stim.color=(-1,-1,-1)
			self._helper._stim.height = val
			self._helper._stim.ori = self.params.helper_orientation 

		# UNTESTED:
		if self.params.bar_flankers:
			[aflank.update(siz, spac=bar_flanker_spacing) for aflank in flankers_all]

	def draw(self):
		[astim.draw() for astim in self._stims]
		if self.params.foveal_helper==True:
			self._helper.draw()	

class FlankedRings(StimSet):
	def __init__(self, name, win, params):
		self.params=params
		self.targets = OddballLetterRing( name="targets", win=win, params=params, radius=(self.params.radius ) )
		if self.params.flankers:
			self.flankers_i = LetterRing( name="flankers_i", win=win, params=params, spokes=self.params.flanker_spokes,
						text=self.params.char_flanker_i, radius=(self.params.radius-self.params.flanker_spacing ) )
			self.flankers_o = LetterRing( name="flankers_o", win=win, params=params, spokes=self.params.flanker_spokes,
						text=self.params.char_flanker_i, radius=(self.params.radius+self.params.flanker_spacing ) )
			self._stims = [self.targets,self.flankers_i,self.flankers_o]
		else:
			self._stims = [self.targets]
	def update(self, val, trialNum):
		[astim.update(val, trialNum) for astim in self._stims]
		self.target_which=self.targets.target_which # propogate from child; TODO: improve!!
		self.target_let=self.targets.target_let
		self.untarget_let=self.targets.untarget_let
		if self.params.flanker_texture:
			[aflanker._stim.setText(self.untarget_let) for aflanker in self.flankers_i._stims]
			[aflanker._stim.setText(self.untarget_let) for aflanker in self.flankers_o._stims]
		if self.params.flankers_alternate:
			[self.flankers_i._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]
			[self.flankers_o._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]
		if self.params.targets_alternate:
			[self.targets._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]
		if self.params.char_flanker_i=='\\':
			coin=np.random.randint(0,2)
			slashes=[ '\\', '/' ]
			[self.flankers_i._stims[n]._stim.setText( slashes[np.random.randint(0,2)] ) for n in np.arange(self.params.num_spokes)]
			[self.flankers_o._stims[n]._stim.setText( slashes[np.random.randint(0,2)] ) for n in np.arange(self.params.num_spokes)]


class FlankerRings(StimSet):
	def __init__(self, name, win, params):
		self.params=params
		#self.targets = OddballLetterRing( name="targets", win=win, params=params, radius=(self.params.radius ) )
		if self.params.flankers:
			self.flankers_i = LetterRing( name="flankers_i", win=win, params=params, spokes=self.params.flanker_spokes,
						text=self.params.char_flanker_i, radius=(self.params.radius-self.params.flanker_spacing ) )
			self.flankers_o = LetterRing( name="flankers_o", win=win, params=params, spokes=self.params.flanker_spokes,
						text=self.params.char_flanker_i, radius=(self.params.radius+self.params.flanker_spacing ) )
			self._stims = [self.flankers_i,self.flankers_o]
		else:
			self._stims = [ ]
	def update(self, val, trialNum):
		[astim.update(val, trialNum) for astim in self._stims]
		#if self.params.flanker_texture:
			#untarget_let=self.targets.untarget_let
			#[aflanker._stim.setText(untarget_let) for aflanker in self.flankers_i._stims]
			#[aflanker._stim.setText(untarget_let) for aflanker in self.flankers_o._stims]
		#if self.params.flankers_alternate:
			#[self.flankers_i._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]
			#[self.flankers_o._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]
		#if self.params.targets_alternate:
			#[self.targets._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]

class TwoLines(Stim):
	def __init__(self, win, name, pos, size, lw=2):
		# Prepend with "_" non-serialized things
		if not(win==None):
			self.win=win
			self._stims=[
				visual.Line( win, start=pos, end=pos, lw=lw ),
				visual.Line( win, start=pos, end=pos, lw=lw )
			]
		else:
			self.win=None
			self._stim=None

		self.name=name
		self.font=font
		self.pos=pos
		self.size=size
		self.lw=lw
		return
	def clone(self):
		anew=TwoLines(self.win, '%sX'%self.name, self.pos, self.size, self.lw)
		return anew
	def __repr__(self):
		rep1='%g, %g,'%(self._stims[0].pos[0], self._stims[0].pos[1])
		rep2='%g, %g,'%(self._stims[1].pos[0], self._stims[1].pos[1])
		return '%s, %s,'%(rep1, rep2)
	# For each trial, set up stuff
	def update(self):
		#[astim.update(val) for astim in self._stims]
		for astim in self._stims:
			xpos=np.random.randint(size)+self.pos[0]
			ypos=np.random.randint(size)+self.pos[1]
			astim.pos=(xpos,ypos)

class Dots(StimSet):
	def __init__(self, win, params, pos, sqsize ):
		stimUL=visual.Circle( win, pos=(pos[0]-sqsize,pos[1]-sqsize), units='pix', fillColor=params.color_def, radius=3 )
		stimUR=visual.Circle( win, pos=(pos[0]-sqsize,pos[1]+sqsize), units='pix', fillColor=params.color_def, radius=3 )
		stimBL=visual.Circle( win, pos=(pos[0]+sqsize,pos[1]-sqsize), units='pix', fillColor=params.color_def, radius=3 )
		stimBR=visual.Circle( win, pos=(pos[0]+sqsize,pos[1]+sqsize), units='pix', fillColor=params.color_def, radius=3 )
		self._stims = [stimUL,stimUR,stimBL,stimBR]

class DotsRing(StimSet):
	def __init__(self,name="",win=None, params=None, sqsize=None, center=(0,0), radius=None):
		self.params=params
		if radius==None:
			radius=self.params.radius
		if sqsize==None:
			sqsize=self.params.sqsize
		self._stims=[Dots(win, params, pos=apos, sqsize=sqsize) for (npos,apos) in
			enumerate(make_ring(center, radius, self.params.num_spokes, phase=self.params.phase)) ] 

################################################
# New Two lines
################################################
# Ts. Normal orientation, then rotations (clockwise)
line2_Ts=[ [[0,2],[1,7]], [[2,8],[3,5]], [[8,6],[7,1]], [[6,0],[3,5]] ]
line2_Ls=[ [[0,2],[0,6]], [[0,2],[2,8]], [[2,8],[8,6]], [[8,6],[6,0]] ]
line2_Ss=[ [[0,8],[1,7]], [[2,6],[1,7]], [[0,8],[3,5]], [[2,6],[3,5]] ] # "scissors"
#line2_Xs=[ [[0,8],[2,6]], [[1,7],[3,5]] ] 
line2_Vs=[ [[0,7],[7,2]], [[2,3],[3,8]], [[6,1],[1,8]], [[5,0],[5,6]] ] 
line2_V2s=[ [[2,8],[8,0]], [[2,8],[2,6]], [[0,6],[0,8]], [[0,6],[2,6]] ] 
# X,Y offsets of each point index
offsets =  [[-1,-1],[0,-1],[1,-1], [-1,0],[0,0],[1,0], [-1,1],[0,1],[1,1]]


def buildpairseq(stims, shuffle=True):
	Nstims=len(stims)
	vals= [(stims[x],stims[y]) for y in xrange(0,Nstims) for x in xrange(0,Nstims) if not(x==y) ]
	if shuffle:
		np.random.shuffle(vals) # modifies in-place
	return vals

class Pairseq(object): 
	def __init__(self, Nstim, shuffle=True ):
		self.seq=buildpairseq(Nstim, shuffle)
		self.itr=iter( self.seq )
	def next(self):
		return self.itr.next()

def getlines(center, radius, (lines1,lines2)):
	# for different types of stimuli, return endpts of two lines
	#lines1,lines2=all2s[which]
	start1=(center[0]+offsets[lines1[0]][0]*radius, center[1]+offsets[lines1[0]][1]*radius )
	end1=(center[0]+offsets[lines1[1]][0]*radius, center[1]+offsets[lines1[1]][1]*radius )
	start2=(center[0]+offsets[lines2[0]][0]*radius, center[1]+offsets[lines2[0]][1]*radius )
	end2=(center[0]+offsets[lines2[1]][0]*radius, center[1]+offsets[lines2[1]][1]*radius )
	return (start1,end1),(start2,end2)

class TwoLines2(StimSet):
	def __init__(self, win, name, pos, size, lineWidth=2 ):
		# Prepend with "_" non-serialized things
		if not(win==None):
			self.win=win
			self._stims=[
				visual.Line( win, start=pos, end=pos, lineWidth=lineWidth, lineColor='white' ),
				visual.Line( win, start=pos, end=pos, lineWidth=lineWidth, lineColor='white' )
			]
		else:
			self.win=None
			self._stim=None

		self.name=name
		self.pos=pos
		self.size=size
		self.lineWidth=lineWidth
		return
	def __repr__(self):
		rep1='%g, %g,'%(self._stims[0].pos[0], self._stims[0].pos[1])
		rep2='%g, %g,'%(self._stims[1].pos[0], self._stims[1].pos[1])
		return '%s, %s,'%(rep1, rep2)
	# For each trial, set up stuff
	def update(self, newlines):
		pos=getlines(self.pos,self.size,newlines)
		#print newlines, pos
		self._stims[0].start=pos[0][0]
		self._stims[0].end=pos[0][1]
		self._stims[1].start=pos[1][0]
		self._stims[1].end=pos[1][1]

class GenericSeqRing(StimSet):
	def __init__(self,name,win, params, stimclass, stimparams, trialseq, radius=None, center=(0,0), jitter_amt=0 ):
		self.params=params
		if radius==None:
			radius=self.params.radius
		try:
			del stimparams['pos'] # just in case
		except:
			pass
		self._stims=[ stimclass(pos=apos, **stimparams) for (npos,apos) in enumerate(make_ring(center, radius, self.params.num_spokes, phase=self.params.phase, jitter_amt=jitter_amt)) ] 
		self.trialseq=trialseq

#win,name="%s_%d"%(name,npos),font=self.params.font, pos=apos, size=self.params.size, text=text, ori=self.params.ori, 
#color=self.params.color_def)
	def update(self,val,trialNum):
		(vals1,vals2)=self.trialseq.itr.next()
		(self.target_element,self.target_which)=oddball_select(self.params.num_spokes)
		# these are used for logging:
		self.target_let=vals1[1]*10+vals1[0]
		self.untarget_let=vals2[1]*10+vals2[0]
		for nstim,astim in enumerate(self._stims):
			if nstim==self.target_element:
				astim.update(vals1)
			else:
				astim.update(vals2)
		# TODO: adjust nominal spacing of flankers.. should not be here I think though.
		#if self.params.variable=="size":
			#[aflanker.setHeight(val) for astim in self._stims]
			#[aflanker.setHeight(val) for astim in self._stims]


def Line2Ring(win, params):
	allseq=np.concatenate( [line2_Ts, line2_Ls, line2_Ss, line2_Vs, line2_V2s] )
	if params.target_sequence=="blocked":
		shuffle=False
	else:
		shuffle=True
	trials=Pairseq(allseq, shuffle=shuffle)
	stimparams={'win':win, 'name':None, 'size':params.size/2.0, 'lineWidth':params.size/5.0 }
	theRing=GenericSeqRing( "line2ring", win, params, TwoLines2, stimparams, trials, radius=params.radius, jitter_amt=10.0)
	return (theRing)
