from numpy import pi,array
import pixlet
import datetime
import time


plotres=True
fullscr = False 
screendim = [1024,768]
screen_height_cm = 30 # in cm
distance_cm=40
fixation_pos=(0,0) 
ntrials = 200
color_def=(1,1,1)
size_noise=1024
screen=1
flipud=True

# For AO Crowding experiment
size_buf=(768,768)
quad_target= pixlet.RANDOM_E #pixlet.RANDOM_BAR
quad_flanker=pixlet.RANDOM_E
bitvals=[1,-1] # background, foreground
char_size=10 # bar size in pixels
#quad_spacing_mar=5*char_size+char_size*5
size_bar=0

# Trial control
mode="infinite" # infinite|erase
method="cs" # "cs|hybrid|staircase"
variable="spacing" # size|duration|spacing
variable_acuity=False # True for size, False for spacing
#variable_type="nominal" # absolute|nominal
#cs_steps=[char_size*5+1, char_size*5+5, char_size*5+10, char_size*5+20, 5000]
#cs_steps=char_size*5+array([10,20,50,100,5000]) #[char_size*5+char_size]
#cs_steps=char_size*5+array([1,2,5,10,5000]) #[char_size*5+char_size]
#cs_steps=[2,4,8] #char_size*5+array([40, 100,200, 5000]) #[char_size*5+char_size]
#cs_steps=[0,1,2,3,4,5,6,7,8,9,10,12,15,100,5000]
#cs_steps=[0,1,2,3,4,5,6,7,8,5000]
#cs_steps=[1,10,100,5000]
#cs_steps=[0,1,3,4,6,7,8,10,15,5000] # without AO, no cheating
#cs_steps=[3,4,6,7,8,9,10,12,15,5000] # without AO (3), no cheating
#cs_steps=[0,1,4,5,7] # without AO (4), no cheating
#cs_steps=[1,3,8,10,15] # without AO (4), no cheating
#cs_steps=[9,11,12,13] # without AO (2), no cheating
#cs_steps=[0,1,3,4,5,6,7,8,15,5000] # with AO, no cheating
#cs_steps=[5,7,10,20,100]
#cs_steps=[0.5,1.0,1000] #unflanked

#cs_steps=[1,2,3,4]
#cs_steps=[0,1,2,4,6,8,10,12,14,16,30,5000] # RS with AO spacing
#cs_steps=[0,6,9,12,15,5000]
cs_steps=[0,6,9,11,13,15,17,20,25,5000] #RS nonAO oct 11th
#cs_steps=[0,3,8,12,16,18,23,27,30,5000] #RS nonAO oct 11th
#cs_steps=[0,1,3,5,7,9,11,13,15,18,25,5000]
#cs_steps=[0]
cs_center=0.0
spacings_blocked = False
#sizes=[3,4,5] # for the Surface at 1000
#sizes=[5,6] # AO, no cheating
#sizes=[7,8] # nonAO, no cheating
#sizes=[3,4,5,6,7] # non AO, no cheating
# 4=20/9.7 5=20/12, 6=20/14.5, 7=20/17 for projector w/150


#sizes=[3,4,5,6,7] # unflanked AO acuity
#sizes=[5,6,7,8,9] # unflanked non acuity

#sizes=[8,9,10,11,12] # projector 300

#sizes=[6, 7, 8, 9, 10,11,12,15] # projector 300, AO
#sizes=[10,11,12,13,14,15,16] # projector 300, no AO 
#sizes=[9,11,13]
#sizes=[7,9,11]
#sizes=[8,9,11]
#sizes=[12, 13, 15]
sizes=[11,14]
#sizes=[200] # for spec ing brightness

# Times in seconds:
trial_time = 0.500
pre_time = 0.0
mask_time = 0.0
between_time = 0.000
noise_on_time=0.000

# Fixation
fixation_char='+'
fixation_size=100 # 80 for projector
fixation_size=200 # 80 for projector

# -----------------------------------
# Legacy stuff for other experiments:
# -----------------------------------

# Ring parameters:
num_spokes=8
radius=300
phase=0/4.0 # or pi/4.0, e.g.
size=30 
char_target_rand=True
char_target='T'
char_untarget='L'
font='Times'
ori=0
flanker_spokes=8
jitter_amt=00.0 # affects all LetterRings

# Flankers
flankers=True
flanker_spacing_nominal=False # If false, pixels, if True, is "X" of size
char_flanker_i='O'
char_flanker_o='O'
char_target='N'
char_untarget='Z'
char_target_rand=True  
target_sequence="balanced"   # "blocked"
orientation = 0 # orientation
letter_set = "Sloan" # or custom
letter_set_custom = ['P', 'R']
flanker_texture=False # Make flanker match untarget
flankers_alternate=False # don't flank everything
flankers_random=True 
targets_alternate=False # don't flank everything

# Helper
foveal_helper=False
helper_orientation = orientation # matches orientation of targets
helper_identity_flanker=False
helper_identity_uninformative=False # uninformative helper
helper_polarity_reverse=False


# To remove:
bar_flankers=False # Bars aren't really tested..

method="hybrid"
do_stair = False
hybrid=True
test_flanker_spacing=False
test_duration=True
cs=True
cs_center=50
#cs_steps=[0] #[-2,-1,0,1,2] #[-20,-10,0,10,20]
test_flanker_spacing=False
test_duration=False
test_size=False
hybrid=False
size_fixed=40

lineWidth=8 # for two-line

# --- Params for our first experiment:
# By default, doing a CS at a single setting, unflanked
flanker_spacing=50.0 
r=276 # radius of target or untarget : 276=8deg
c1_dist=45
c2_dist=100
c3_dist=150
cc_dist=60

size_cs_logspace=(10,60,5)

do_spacings=True
spacings_cs = [5000, 80, 100, 120, 150]
spacings_blocked = False

# Change the things below for each subject/condition
# --------------------------------------------
# Conditions:
#   0==calibration for size reading
#   1==calibration for size reading, crowded
#   0H==calibration for size reading, with helper
#   C==Crowded near
#   CC==Crowded medium
#   CCC==Crowded far
#   U=Uncrowded
#   HC=Crowded (2), with helper
#   HCU=Crowded (2), With uninformative helper
#   H=Uncrowded, helper
#   HU=Uncrowded, uninformative helper
#   S=Sizes, constant stimuli

#   16T16=Texture: 16X16, matching
#   16X16=16 spokes, 16 X flankers
#   16X8=16 spokes, 8 X flankers
#   16T8=16 spokes, 8 texture flankers
#   8T16=16 spokes, 8 texture flankers
#   8T8=16 spokes, 8 texture flankers
#   8X8=8 spokes, 8 X flankers
#   16T16C=Texture: 16X16, matching, closer-spacing

SubjectName = 'RS'
#condition="2L-4C"
#condition="16X16"
condition="ee_flanked_spacing_nonao"
cs_center=50

timestarted=datetime.datetime.now().strftime("%A %d %B %Y %I:%M%p")
