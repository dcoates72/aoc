import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import newfit
from newfit import ci95
from sklearn.neighbors.kde import KernelDensity

def show_param(p,res,doplot=False):
    vals=(res[-1])[p]
    if doplot:
        pd.DataFrame(vals).plot(kind='density')
        yl=plt.ylim()
    men=np.mean(vals)
    ci=ci95(vals)
    if doplot:
        plt.plot([men,men],yl,'r')
        plt.plot([ci[0],ci[0]],yl,'g')

    #mle=res[0][p]

    if doplot:
        plt.plot([mle,mle],yl,'b')

    # Hm, this doesn't really give the Peak of the Gaussian as I was hoping..
    # Maybe the method isn't as good as the one in Pandas.. Automatci bw selection?
    kde = KernelDensity(kernel='gaussian', bandwidth=0.1).fit(vals[:,np.newaxis])
    xr=np.linspace( np.min(vals), np.max(vals),1000 )
    scores=kde.score_samples(xr[:,np.newaxis])
    peak=np.max(scores)
    idx=np.arange(1000)[scores==peak][0]

    if doplot:
        plt.plot([xr[idx],xr[idx]],yl,'k')
    #plt.plot(xr,scores*10)

    return ci[0],scores,idx,xr[idx],xr
